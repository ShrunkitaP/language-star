//
//  DashboardScreenViewController.swift
//  Learning App
//
//  Created by HPL on 05/10/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit
import CoreData
import SWRevealViewController

class DashboardScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var dashboardView: UIView!
    @IBOutlet var dashboardTableView: UITableView!
    @IBOutlet var dashboardLabel: UILabel!
    let viewpop :UIView = UIView()
    
    var questionsData : NSMutableArray = NSMutableArray()
    var starArray : NSMutableArray = NSMutableArray()
    var DataArrayDash : NSDictionary = NSDictionary()
    var quizTwoArray : NSMutableArray = NSMutableArray()
    var quizThreeArray : NSMutableArray = NSMutableArray()
//    var storeData : NSMutableArray = NSMutableArray()
    var langIdStoreArray : NSMutableArray = NSMutableArray()
    var totalCountArray : NSMutableArray = NSMutableArray()
    var user_store_data = [String:String]()
    var delegateFlag = false
    var appdelegate: AppDelegate?
    var starDashboardCount : Int = 0
    var isClick = false
    var levelName = ["Intermediate Level", "Advance Level", "Expert Level"]
    var levelData: String = ""
    var jsonData : NSDictionary = NSDictionary()
    let defaultSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    var dataTask: NSURLSessionDataTask?
    var dataobj = UserDatabase()
    var dataArray :NSArray = NSArray()
    var poplpLabel : UILabel!
    var popImage : UIImageView!
    var level_id = String()
    var lang_id = String()
    var quiz_no = String()
    var medal_dash : String?
    var medal_quiz_check = "0"
    var i = 0
    var total = Int()
    var finalCount = 9
    var updateStar : AnyObject = ""
    var sumOne = Int()
    var sumTwo = Int()
    var sumThree = Int()
    var quizCount : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        medal_quiz_check = "0"
        lang_id = NSUserDefaults.standardUserDefaults().objectForKey("lang_id")! as! String
        print(lang_id)
        quizCount = NSUserDefaults.standardUserDefaults().objectForKey("quiz_no") as? Int
        print(quizCount)
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        self.navigationItem.hidesBackButton = true;
     //   KLCPopup.dismissAllPopups()
         self.dashboardTableView!.reloadData()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool){
        
        super.viewWillAppear(true)
        self.navigationController!.setNavigationBarHidden(true, animated: false)
        lang_id = NSUserDefaults.standardUserDefaults().objectForKey("lang_id")! as! String
        print(lang_id)
        dataArray = dataobj.fetchAllRecord(String(lang_id) as NSString)
        print(dataArray)
     //   KLCPopup.dismissAllPopups()
    
        self.dashboardTableView!.reloadData()
 
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
    //     KLCPopup.dismissAllPopups()
        delegateFlag = false
    }
    
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
        print(DataArrayDash)
      //  KLCPopup.dismissAllPopups()
       
            if dataArray.count != 0
            {
            for i in 0...dataArray.count-1{
                
                print(user_store_data)
                let cord_data = dataArray.objectAtIndex(i)
                print(cord_data.valueForKey("lang_id")!)
                print(cord_data.valueForKey("level_id")!)
                print(cord_data.valueForKey("quiz_no")!)
                print(cord_data.valueForKey("star")! as! String)
                print(cord_data.valueForKey("medal")!)
                user_store_data["lang_id"] = cord_data.valueForKey("lang_id") as? String
                user_store_data["level_id"] = cord_data.valueForKey("level_id") as? String
                user_store_data["quiz_no"] = String(cord_data.valueForKey("quiz_no")!)
                user_store_data["star"] = cord_data.valueForKey("star") as? String
                user_store_data["medal"] = String(cord_data.valueForKey("medal")!)
                self.dashboardTableView.reloadData()
                starFetch(user_store_data, indexpath: Int(user_store_data["level_id"]!)!)
                congrats(Int(user_store_data["level_id"]!)!)
                }
            
            }else
            {
                self.firstButtonEnable()
            }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! dashboradCell
        cell.button1.tag = indexPath.row
        cell.button2.tag = indexPath.row
        cell.button3.tag = indexPath.row
        
        if (indexPath.row == 0){
            cell.levelLabel.text = "Intermediate Level"
            //cell.dashboardLevelView.backgroundColor = UIColor(red: 72/255.0, green: 34/255, blue: 3.0, alpha: 1)


        } else if (indexPath.row == 1){
             cell.levelLabel.text = "Advance Level"
            cell.dashboardLevelView.backgroundColor = UIColor(red: 42/255.0, green: 52/255.0, blue: 66/255.0, alpha: 1)

        } else
        {
            cell.levelLabel.text = "Expert Level"
            cell.dashboardLevelView.backgroundColor = UIColor(red: 118/255.0, green: 5/255.0, blue: 6/255.0, alpha: 1)
        }
        
               cell.button1.accessibilityIdentifier = String(indexPath.row)
        cell.button1.addTarget(self, action: #selector(DashboardScreenViewController.intermediateLevelButtonOne(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        
       
               cell.button2.accessibilityIdentifier = String(indexPath.row)
        cell.button2.addTarget(self, action: #selector(DashboardScreenViewController.intermediateLevelButtonTwo(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        
       
             cell.button3.accessibilityIdentifier = String(indexPath.row)
        cell.button3.addTarget(self, action: #selector(DashboardScreenViewController.intermediateLevelButtonThree(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        

        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        return cell
        
    }

    
    @IBAction func Query(sender: UIButton) {
//        let user_arr : NSArray = NSBundle.mainBundle().loadNibNamed("PopupViewController", owner: self, options: nil)!  as NSArray
//        let view :UIView = user_arr.objectAtIndex(0) as! UIView
//        poplpLabel = (view.viewWithTag(2)! as! UILabel)
//        popImage = (view.viewWithTag(1)! as! UIImageView)
//        popImage.image = UIImage(named: "gold_badge_1x")
//        poplpLabel.text = "You Earned a Gold Medal"
//        poplpLabel.textColor = UIColor.redColor()
//        let popup = KLCPopup(contentView: view , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
//        popup.show()
//        NSUserDefaults.standardUserDefaults().setObject("1", forKey: "FirstPOP")
//        popup.dismiss(true)
//        popup.dismissPresentingPopup()

    }
    
    
    func starFetch(user_detail : [String:String],indexpath:Int)
    {
        print(user_detail)
        var cell_indexPath = NSIndexPath(forRow: indexpath, inSection: 0)
        let tableViewCell = self.dashboardTableView.cellForRowAtIndexPath(cell_indexPath) as! dashboradCell
        starDashboardCount = Int(user_detail["star"]!)!
        quiz_no = user_detail["quiz_no"] != nil ? user_detail["quiz_no"]! : user_detail["quizKey"]!
        lang_id = user_detail["lang_id"] != nil ? user_detail["lang_id"]! : user_detail["langKey"]!
        medal_dash = user_detail["medal"] != nil ? user_detail["medal"]! : user_detail["medalKey"]!
        print(medal_dash)
        if (cell_indexPath.row == 0){
//            medal_dash = "0"
            if (quiz_no == "1")
            {
                tableViewCell.buttonOneImage.image = UIImage(named: "1x")
                if starDashboardCount == 0{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2.enabled = false
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 1
                {
                    tableViewCell.button1SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2.enabled = false
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 2{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.buttonTwoImage.image = UIImage(named: "1x")
                    tableViewCell.button2.enabled = true
                }else if starDashboardCount == 3{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "small_star_1x")
                    tableViewCell.buttonTwoImage.image = UIImage(named: "1x")
                    tableViewCell.button2.enabled = true
                }
                sumOne = self.starDashboardCount
                print(starDashboardCount)
            }
            if quiz_no == "2"{
                if starDashboardCount == 0{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 1{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 2{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button3.enabled = true
                }else if starDashboardCount == 3 {
                    tableViewCell.button2SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button3.enabled = true
                }
                sumTwo = self.starDashboardCount
                print(starDashboardCount)
            }
            if quiz_no == "3"{
                if starDashboardCount == 0{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1.enabled = false
                }else if starDashboardCount == 1{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1.enabled = true
                }else if starDashboardCount == 2{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button1.enabled = true
                }else if starDashboardCount == 3{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    //                    tableViewCell.button1.enabled = true
                }
                sumThree = self.starDashboardCount
                print(starDashboardCount)
            }
            
            total = sumOne + sumTwo + sumThree
            print(total)
        } else if (cell_indexPath.row == 1){
//            medal_dash = "0"
            tableViewCell.button1.enabled = true
            if quiz_no == "4"{
                if starDashboardCount == 0{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2.enabled = false
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 1{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2.enabled = false
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 2{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.buttonTwoImage.image = UIImage(named: "1x")
                    tableViewCell.button2.enabled = true
                }else if starDashboardCount == 3{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "small_star_1x")
                    tableViewCell.buttonTwoImage.image = UIImage(named: "1x")
                    tableViewCell.button2.enabled = true
                }
                sumOne = self.starDashboardCount
                print(starDashboardCount)
            }
            if quiz_no == "5"{
                if starDashboardCount == 0{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 1{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 2{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button3.enabled = true
                }else if starDashboardCount == 3{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "small_star_1x")
                    tableViewCell.buttonTwoImage.image = UIImage(named: "1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button3.enabled = true
                }
                sumTwo = self.starDashboardCount
                print(starDashboardCount)
                
            }
            if quiz_no == "6"{
                if starDashboardCount == 0{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1.enabled = false
                }else if starDashboardCount == 1{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1.enabled = true
                }else if starDashboardCount == 2{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button1.enabled = true
                }else if starDashboardCount == 3{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button1.enabled = true
                }
                sumThree = self.starDashboardCount
                print(starDashboardCount)
                
            }
            total = sumOne + sumTwo + sumThree
            print(total)
            
        }else if (cell_indexPath.row == 2){
//            medal_dash = "0"
            tableViewCell.button1.enabled = true
            
            if quiz_no == "7" {
                if starDashboardCount == 0{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2.enabled = false
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 1
                {
                    tableViewCell.button1SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2.enabled = false
                }else if starDashboardCount == 2{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.buttonTwoImage.image = UIImage(named: "1x")
                    tableViewCell.button2.enabled = true
                }else if starDashboardCount == 3{
                    tableViewCell.button1SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button1SmallStar2.image = UIImage(named: "small_star_1x")
                    tableViewCell.buttonTwoImage.image = UIImage(named: "1x")
                    tableViewCell.button2.enabled = true
                }
                sumOne = self.starDashboardCount
                print(starDashboardCount)
            }
            if quiz_no == "8"{
                if starDashboardCount == 0{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 1{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3.enabled = false
                }else if starDashboardCount == 2{
                    tableViewCell.button2SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button3.enabled = true
                }else if starDashboardCount == 3 {
                    tableViewCell.button2SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button2SmallStar2.image = UIImage(named: "small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button3.enabled = true
                }
                sumTwo = self.starDashboardCount
                print(starDashboardCount)
                
            }
            if quiz_no == "9"{
                if starDashboardCount == 0{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1.enabled = false
                }else if starDashboardCount == 1{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.button1.enabled = true
                }else if starDashboardCount == 2{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "grey_small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                    tableViewCell.button1.enabled = true
                }else if starDashboardCount == 3{
                    tableViewCell.button3SmallStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3BigStar1.image = UIImage(named: "small_star_1x")
                    tableViewCell.button3SmallStar2.image = UIImage(named: "small_star_1x")
                    tableViewCell.buttonThreeImage.image = UIImage(named: "1x")
                }
                sumThree = self.starDashboardCount
                print(starDashboardCount)
                
            }
            total = sumOne + sumTwo + sumThree
            print(total)
            
        }
        
        let tableViewCell3 = self.dashboardTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 2,inSection: 0)) as! dashboradCell
        let tableViewCell2 = self.dashboardTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1,inSection: 0)) as! dashboradCell
        tableViewCell.medalImage.hidden = true
        if quiz_no == "1" || quiz_no == "2"{
            if total != finalCount{
                tableViewCell2.button1.enabled = false
                tableViewCell2.button2.enabled = false
                tableViewCell2.button3.enabled = false
                tableViewCell3.button1.enabled = false
                tableViewCell3.button2.enabled = false
                tableViewCell3.button3.enabled = false
            }
            else if quiz_no == "4" || quiz_no == "5"{
                if total != finalCount{
                    tableViewCell3.button1.enabled = false
                    tableViewCell3.button2.enabled = false
                    tableViewCell3.button3.enabled = false
                }
                
            }
        }
        
        
        if quiz_no == "3"{
            
            cell_indexPath = NSIndexPath(forRow: 0, inSection: 0)
            
            if total == finalCount
            {
                tableViewCell2.button1.enabled = true
                tableViewCell2.button2.enabled = false
                tableViewCell2.button3.enabled = false
                tableViewCell3.button1.enabled = false
                tableViewCell3.button2.enabled = false
                tableViewCell3.button3.enabled = false
                tableViewCell2.buttonOneImage.image = UIImage(named: "1x")
                tableViewCell.medalImage.hidden = false
                tableViewCell.medalImage.image = UIImage(named: "congratulations_bronze_1X")
            }
            else
            {
                let Cust = CustomClass()
                var alert = UIAlertController( )
                alert = Cust.displayMyAlertMessage("To Unlock Next level Earn 9 Stars from This level")
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
        } else if quiz_no == "6"{
            cell_indexPath = NSIndexPath(forRow: 1, inSection: 0)
            if total == finalCount
            {
                
                tableViewCell3.button1.enabled = true
                tableViewCell3.button2.enabled = false
                tableViewCell3.button3.enabled = false
                tableViewCell3.buttonOneImage.image = UIImage(named: "1x")
                tableViewCell.medalImage.hidden = false
                tableViewCell.medalImage.image = UIImage(named: "congratulations_silver_1X")
            }
            else
            {
                let Cust = CustomClass()
                var alert = UIAlertController( )
                alert = Cust.displayMyAlertMessage("To Unlock Expert level Earn 9 Stars from This level")
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
        } else if quiz_no == "9"{
            cell_indexPath = NSIndexPath(forRow: 2, inSection: 0)
            if total != finalCount{
                let Cust = CustomClass()
                var alert = UIAlertController( )
                alert = Cust.displayMyAlertMessage("Please Complete The Level")
                self.presentViewController(alert, animated: true, completion: nil)
            } else
            {
                tableViewCell.medalImage.hidden = false
                tableViewCell.medalImage.image = UIImage(named: "gold_badge_1x")
                
            }
        }
//        if (cell_indexPath.row == 2){
//                 if quiz_no == "9" && total == finalCount{
//                    
//                    self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash)
//                    
//                if medal_dash == "1"
//                {
//                self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_quiz_check)
//                    medal_dash = "0"
//                    print(medal_dash)
//                    self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_quiz_check)
//                    let user_arr : NSArray = NSBundle.mainBundle().loadNibNamed("PopupViewController", owner: self, options: nil)!  as NSArray
//                    let view :UIView = user_arr.objectAtIndex(0) as! UIView
//                    poplpLabel = (view.viewWithTag(2)! as! UILabel)
//                    popImage = (view.viewWithTag(1)! as! UIImageView)
//                    popImage.image = UIImage(named: "gold_badge_1x")
//                    poplpLabel.text = "You Earned a Gold Medal"
//                    poplpLabel.textColor = UIColor.redColor()
//                    let popup = KLCPopup(contentView: view , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
//                    popup.show()
//                        NSUserDefaults.standardUserDefaults().setObject("1", forKey: "FirstPOP")
//                    popup.dismiss(true)
//                    popup.dismissPresentingPopup()
//    }
//                   // }
//        }
//        }else if (cell_indexPath.row == 1)
//                 {
//                    if quiz_no == "6" && total == finalCount{
//                   self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash)
//                    if medal_dash == "1"
//                {
//                    medal_dash = "0"
//                    self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash)
//                    NSUserDefaults.standardUserDefaults().setObject("2", forKey: "FirstPOP")
//                    let user_arr : NSArray = NSBundle.mainBundle().loadNibNamed("PopupViewController", owner: self, options: nil)!  as NSArray
//                    let view :UIView = user_arr.objectAtIndex(0) as! UIView
//                    poplpLabel = (view.viewWithTag(2)! as! UILabel)
//                    popImage = (view.viewWithTag(1)! as! UIImageView)
//                    popImage.image = UIImage(named: "congratulations_silver_1X")
//                    poplpLabel.text = "You Earned a Silver Medal"
//                    let popup = KLCPopup(contentView: view , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
//                    popup.show()
//                     popup.dismiss(true)
////                                       }
//                        }
//                    }
//                 }else if (cell_indexPath.row == 0){
//                 if quiz_no == "3" && total == finalCount{
//                    
//                    self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash)
//                    if  medal_dash == "1"
//              {
//                medal_dash = "0"
//                self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash)
//                let user_arr : NSArray = NSBundle.mainBundle().loadNibNamed("PopupViewController", owner: self, options: nil)!  as NSArray
//                let view :UIView = user_arr.objectAtIndex(0) as! UIView
//                poplpLabel = (view.viewWithTag(2)! as! UILabel)
//                popImage = (view.viewWithTag(1)! as! UIImageView)
//                popImage.image = UIImage(named: "congratulations_bronze_1X")
//                poplpLabel.text = "You Earned a Bronze Medal"
//                poplpLabel.textColor = UIColor.redColor()
//                let popup = KLCPopup(contentView: view , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
//                popup.show()
//                NSUserDefaults.standardUserDefaults().setObject("1", forKey: "FirstPOP")
//                 popup.dismiss(true)
//               
////               }
//                    }
//            
//            }
//        }
     
        
    }
    
    func congrats(indexpath:Int){
        let cell_indexPath = NSIndexPath(forRow: indexpath, inSection: 0)
        let tableViewCell = self.dashboardTableView.cellForRowAtIndexPath(cell_indexPath) as! dashboradCell
      //  self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash!)
        if (cell_indexPath.row == 2){
            if quiz_no == "9" && total == finalCount{
         //       self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash!)
                if medal_dash == "1"
                {
                    medal_dash = "0"
                    print(medal_dash)
                    self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash!)
                    self.dataobj.fetchAllRecord(String(lang_id) as NSString)
                    let user_arr : NSArray = NSBundle.mainBundle().loadNibNamed("PopupViewController", owner: self, options: nil)!  as NSArray
                    let view :UIView = user_arr.objectAtIndex(0) as! UIView
                    poplpLabel = (view.viewWithTag(2)! as! UILabel)
                    popImage = (view.viewWithTag(1)! as! UIImageView)
                    popImage.image = UIImage(named: "gold_badge_1x")
                    poplpLabel.text = "You Earned a Gold Medal"
                    poplpLabel.textColor = UIColor.redColor()
                    let popup = KLCPopup(contentView: view , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
                    popup.show()
                  //  NSUserDefaults.standardUserDefaults().setObject("1", forKey: "FirstPOP")
                    popup.dismiss(true)
                    popup.dismissPresentingPopup()
                }
                // }
            }
        }
        else if (cell_indexPath.row == 1)
        {
            if quiz_no == "6" && total == finalCount{
         //       self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash!)
                if medal_dash == "1"
                {
                    medal_dash = "0"
                    self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash!)
                    print(medal_dash)
                    self.dataobj.fetchAllRecord(String(lang_id) as NSString)
                  //  NSUserDefaults.standardUserDefaults().setObject("2", forKey: "FirstPOP")
                    let user_arr : NSArray = NSBundle.mainBundle().loadNibNamed("PopupViewController", owner: self, options: nil)!  as NSArray
                    let view :UIView = user_arr.objectAtIndex(0) as! UIView
                    poplpLabel = (view.viewWithTag(2)! as! UILabel)
                    popImage = (view.viewWithTag(1)! as! UIImageView)
                    popImage.image = UIImage(named: "congratulations_silver_1X")
                    poplpLabel.text = "You Earned a Silver Medal"
                    let popup = KLCPopup(contentView: view , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
                    popup.show()
                    popup.dismiss(true)
                    //                                       }
                }
            }
        }else if (cell_indexPath.row == 0){
            if quiz_no == "3" && total == finalCount{
//                self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash)
                if  medal_dash == "1"
                {
                    medal_dash = "0"
                    //self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash)
                    self.dataobj.updateTable(quiz_no, stars: String(starDashboardCount), langid: String(lang_id), medal: medal_dash!)
                    self.dataobj.fetchAllRecord(String(lang_id) as NSString)
                    var fetchArray : NSArray = NSArray()
                    fetchArray = self.dataobj.fetchAllRecord(String(lang_id) as NSString)
                    print(fetchArray)
                    
                    let user_arr : NSArray = NSBundle.mainBundle().loadNibNamed("PopupViewController", owner: self, options: nil)!  as NSArray
                    let view :UIView = user_arr.objectAtIndex(0) as! UIView
                    poplpLabel = (view.viewWithTag(2)! as! UILabel)
                    popImage = (view.viewWithTag(1)! as! UIImageView)
                    popImage.image = UIImage(named: "congratulations_bronze_1X")
                    poplpLabel.text = "You Earned a Bronze Medal"
                    poplpLabel.textColor = UIColor.redColor()
                    let popup = KLCPopup(contentView: view , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
                    popup.show()
                   // NSUserDefaults.standardUserDefaults().setObject("1", forKey: "FirstPOP")
                    popup.dismiss(true)
                    
                    //               }
                }
                
            }
        }
        
        
    }

    
    func  firstButtonEnable() {
        var cell_indexPath = NSIndexPath(forRow: 0, inSection: 0)
        var tableViewCell = self.dashboardTableView.cellForRowAtIndexPath(cell_indexPath) as! dashboradCell
        tableViewCell.buttonOneImage.image = UIImage(named: "1x")
        tableViewCell.button1.enabled = true
        tableViewCell.button2.enabled = false
        tableViewCell.button3.enabled = false
        cell_indexPath = NSIndexPath(forRow: 1, inSection: 0)
        tableViewCell = self.dashboardTableView.cellForRowAtIndexPath(cell_indexPath) as! dashboradCell
        tableViewCell.button1.enabled = false
        tableViewCell.button2.enabled = false
        tableViewCell.button3.enabled = false
        cell_indexPath = NSIndexPath(forRow: 2, inSection: 0)
        tableViewCell = self.dashboardTableView.cellForRowAtIndexPath(cell_indexPath) as! dashboradCell
        tableViewCell.button1.enabled = false
        tableViewCell.button2.enabled = false
        tableViewCell.button3.enabled = false
    }
    
    
     func intermediateLevelButtonOne(sender: UIButton)
     {
         appdelegate!.showHUD(self.view)
        let buttonRow = Int(sender.accessibilityIdentifier!)
        print(buttonRow!)
        let indexPath: NSIndexPath = NSIndexPath(forRow: buttonRow!, inSection: 0)
        let tableViewCell : dashboradCell! = self.dashboardTableView.cellForRowAtIndexPath(indexPath) as? dashboradCell
        tableViewCell.button1.tag == 0
        print(tableViewCell.button1.tag)
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration)
        var params:NSDictionary = NSDictionary()
        let userDefault = NSUserDefaults.standardUserDefaults()
        let nativelangId = userDefault.objectForKey("lang_id") as! String
        
       
//        print(self.quizTwoArray)

        if(buttonRow == 0 && tableViewCell.button1.tag == 0)
            
        {
            params = ["native_lang_id" : nativelangId, "level" : "intermediate"]
            level_id = "0"
            quiz_no = "1"
            
        }
        else if (buttonRow == 1 && tableViewCell.button1.tag == 1 )
        {
            params = ["native_lang_id" : nativelangId, "level" : "advance"]
            level_id = "1"
            quiz_no = "4"
            
        }
        else if (buttonRow == 2 && tableViewCell.button1.tag == 2)
        {
            params = ["native_lang_id" : nativelangId, "level" : "expert"]
            level_id = "2"
            quiz_no = "7"
        }
        else
        {
            print("error")
        }
    
        if Reachability.isConnectedToNetwork() == true
        {
       
        let url = NSURL(string:"http://languagestar.net/admin/public/getQuestionList")
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        do
        {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString = NSString(data:jsonData, encoding: NSUTF8StringEncoding)
            request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding)
        }
        catch //let error as NSError
        {
            print(error)
            //jsonData = NSDictionary()
        }
        let task = session.dataTaskWithRequest(request) {
            data, response, error in
            do
            {
                self.jsonData = try NSJSONSerialization.JSONObjectWithData(data!,
                options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                print(self.jsonData) // print dictionary
                print(self.jsonData.count)
                if (self.jsonData.objectForKey("data")!.isKindOfClass(NSNull)){
                    dispatch_async(dispatch_get_main_queue()) {
                        self.appdelegate?.dismissView(self.view)
                    }
//                    self.appdelegate?.dismissView(self.view)
                    let Cust = CustomClass()
                    var alert = UIAlertController( )
                    alert = Cust.displayMyAlertMessage("No Data Found")
                    self.presentViewController(alert, animated: true, completion: nil)

                }else{
                self.questionsData.removeAllObjects()
                    print(self.questionsData)
                 self.questionsData.setArray(self.jsonData.objectForKey("data")!.objectAtIndex(0) as! [AnyObject])
                print(self.questionsData.count)
                   
                    print(self.questionsData)
                     if self.questionsData.count == 16{
                        dispatch_async(dispatch_get_main_queue()) {
                    self.appdelegate?.dismissView(self.view)
                    self.performSegueWithIdentifier("QuizView", sender: self)
                }
                    }
                    else
                     {
                    let Cust = CustomClass()
                    var alert = UIAlertController( )
                    alert = Cust.displayMyAlertMessage("No Data Found")
                    self.presentViewController(alert, animated: true, completion: nil)
                        dispatch_async(dispatch_get_main_queue()) {
                            self.appdelegate?.dismissView(self.view)
                            self.performSegueWithIdentifier("QuizView", sender: self)
                        }

                    }
                }

            }catch {
                    print("error serializing JSON: \(error)")
                  }
            
              
            
            if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    print("response was not 200: \(response)")
                    print(NSString(data: request.HTTPBody!, encoding:NSUTF8StringEncoding)!)
                }
            }
            
        }
        
        task.resume()
        }else{
           self.appdelegate?.dismissView(self.view)
            let Cust = CustomClass()
            var alert = UIAlertController( )
            alert = Cust.displayMyAlertMessage("Please check your internet connection.")
            self.presentViewController(alert, animated: true, completion: nil)

        }
}
    
    
    
     func intermediateLevelButtonTwo(sender: UIButton) {
        
        appdelegate!.showHUD(self.view)
        
        let buttonRow = Int(sender.accessibilityIdentifier!)
        print(buttonRow!)
        let indexPath: NSIndexPath = NSIndexPath(forRow: buttonRow!, inSection: 0)
        let tableViewCell : dashboradCell! = self.dashboardTableView.cellForRowAtIndexPath(indexPath) as? dashboradCell
        tableViewCell.button2.tag == 0
        print(tableViewCell.button2.tag)
        
        var params:NSDictionary = NSDictionary()
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration)
        let userDefault = NSUserDefaults.standardUserDefaults()
        let nativelangId = userDefault.objectForKey("lang_id")

        if(buttonRow == 0 && tableViewCell.button2.tag == 0) {
            
            params = ["native_lang_id" : nativelangId!, "level" : "intermediate"]
            level_id = "0"
            quiz_no = "2"
            
            
        }else if (buttonRow == 1 && tableViewCell.button2.tag == 1)
        {
            params = ["native_lang_id" : nativelangId!, "level" : "advance"]
            level_id = "1"
            quiz_no = "5"

        }else if (buttonRow == 2 && tableViewCell.button2.tag == 2)
        {
            params = ["native_lang_id" : nativelangId!, "level" : "expert"]
            level_id = "2"
            quiz_no = "8"

        }
        
        if Reachability.isConnectedToNetwork() == true
        {

        
        let url = NSURL(string:"http://languagestar.net/admin/public/getQuestionList")
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        do
        {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString = NSString(data:jsonData, encoding: NSUTF8StringEncoding)
            request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding)
            
        }
        catch //let error as NSError
        {
            print(error)
        }
        
        let task = session.dataTaskWithRequest(request) {
            data, response, error in
            do
            {
                self.jsonData = try NSJSONSerialization.JSONObjectWithData(data!,
                    options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                print(self.jsonData) // print dictionary
                if (self.jsonData.objectForKey("data")!.isKindOfClass(NSNull)){
                    dispatch_async(dispatch_get_main_queue()) {
                        self.appdelegate?.dismissView(self.view)
                    }
                    let Cust = CustomClass()
                    var alert = UIAlertController( )
                    alert = Cust.displayMyAlertMessage("No Data Found")
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }else{
                    self.questionsData.removeAllObjects()
                    print(self.questionsData)
                    self.questionsData.setArray(self.jsonData.objectForKey("data")!.objectAtIndex(1) as! [AnyObject])
                    print(self.questionsData)
                    if self.questionsData.count == 16{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.appdelegate?.dismissView(self.view)
                            self.performSegueWithIdentifier("QuizView", sender: self)
                        }
                    }
                    else
                    {
                        let Cust = CustomClass()
                        var alert = UIAlertController( )
                        alert = Cust.displayMyAlertMessage("No Data Found")
                        self.presentViewController(alert, animated: true, completion: nil)
                        dispatch_async(dispatch_get_main_queue()) {
                            self.appdelegate?.dismissView(self.view)
                            self.performSegueWithIdentifier("QuizView", sender: self)
                        }
                        
                    }
                }
                
            }catch {
                print("error serializing JSON: \(error)")
            }
                
            if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    print("response was not 200: \(response)")
                    print(NSString(data: request.HTTPBody!, encoding:NSUTF8StringEncoding)!)
                }
            }
            
        }
        
        task.resume()
            
        }else{
           self.appdelegate?.dismissView(self.view)
            let Cust = CustomClass()
            var alert = UIAlertController( )
            alert = Cust.displayMyAlertMessage("Please check your internet connection.")
            self.presentViewController(alert, animated: true, completion: nil)
 
        }
        
    }
    
    
    func intermediateLevelButtonThree(sender: UIButton) {
        
        
        appdelegate!.showHUD(self.view)
        
        let buttonRow = Int(sender.accessibilityIdentifier!)
        print(buttonRow!)
        let indexPath: NSIndexPath = NSIndexPath(forRow: buttonRow!, inSection: 0)
        let tableViewCell : dashboradCell! = self.dashboardTableView.cellForRowAtIndexPath(indexPath) as? dashboradCell
        tableViewCell.button3.tag == 0
        print(tableViewCell.button3.tag)
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration)
        var params:NSDictionary = NSDictionary()
        let userDefault = NSUserDefaults.standardUserDefaults()
        let nativelangId = userDefault.objectForKey("lang_id")

        
//         self.quizThreeArray.setArray(self.questionsData as [AnyObject])

        if(buttonRow == 0 && tableViewCell.button3.tag == 0) {
            
            params = ["native_lang_id" : nativelangId!, "level" : "intermediate"]
            level_id = "0"
            quiz_no = "3"
            
            
        }else if (buttonRow == 1 && tableViewCell.button3.tag == 1)
        {
            params = ["native_lang_id" : nativelangId!, "level" : "advance"]
            level_id = "1"
            quiz_no = "6"
            
        }else if (buttonRow == 2 && tableViewCell.button3.tag == 2)
        {
            params = ["native_lang_id" : nativelangId!, "level" : "expert"]
            level_id = "2"
            quiz_no = "9"
            
        }
        if Reachability.isConnectedToNetwork() == true
        {

        
        let url = NSURL(string:"http://languagestar.net/admin/public/getQuestionList")
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        
        do
        {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString = NSString(data:jsonData, encoding: NSUTF8StringEncoding)
            request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding)
            
        }
            
        catch //let error as NSError
        {
            print(error)
        }
        
        
        let task = session.dataTaskWithRequest(request) {
            data, response, error in
            do
            {
                self.jsonData = try NSJSONSerialization.JSONObjectWithData(data!,
                    options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                print(self.jsonData) // print dictionary
                if (self.jsonData.objectForKey("data")!.isKindOfClass(NSNull)){
                    dispatch_async(dispatch_get_main_queue()) {
                        self.appdelegate?.dismissView(self.view)
                    }
                    let Cust = CustomClass()
                    var alert = UIAlertController( )
                    alert = Cust.displayMyAlertMessage("No Data Found")
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }else{
                    self.questionsData.removeAllObjects()
                    print(self.questionsData)
                    self.questionsData.setArray(self.jsonData.objectForKey("data")!.objectAtIndex(2) as! [AnyObject])
                    print(self.questionsData)
                    if self.questionsData.count == 16{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.appdelegate?.dismissView(self.view)
                            self.performSegueWithIdentifier("QuizView", sender: self)
                        }
                    }
                    else
                    {
                        let Cust = CustomClass()
                        var alert = UIAlertController( )
                        alert = Cust.displayMyAlertMessage("No Data Found")
                        self.presentViewController(alert, animated: true, completion: nil)
                        dispatch_async(dispatch_get_main_queue()) {
                            self.appdelegate?.dismissView(self.view)
                            self.performSegueWithIdentifier("QuizView", sender: self)
                        }
                        
                    }
                }
                
            } catch {
                print("error serializing JSON: \(error)")
            }
            
            if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    print("response was not 200: \(response)")
                    print(NSString(data: request.HTTPBody!, encoding:NSUTF8StringEncoding)!)
                    
                }
            }
            
        }
        
        task.resume()
            
    }else{
            self.appdelegate?.dismissView(self.view)
            let Cust = CustomClass()
            var alert = UIAlertController( )
            alert = Cust.displayMyAlertMessage("Please check your internet connection.")
            self.presentViewController(alert, animated: true, completion: nil)

        }
    }
    
    
    @IBAction func backButton(sender: UIButton) {
        let destination = self.storyboard?.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.navigationController?.pushViewController(destination, animated: false)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "QuizView")
        {
            let quizV : QuizViewController =  (segue.destinationViewController as! QuizViewController)
            quizV.quizLevelArray = questionsData
            quizV.databaseDictionary = DataArrayDash
            quizV.levelId = level_id
            quizV.langId = lang_id
            quizV.quizCount = quizCount
            quizV.quizId = quiz_no
            quizV.starCount = starDashboardCount
            
        }
        
        
    }
    
}
    


        








 
                    
                    
   
            

    
    
    
    
    
  
