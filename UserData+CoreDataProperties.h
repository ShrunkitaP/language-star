//
//  UserData+CoreDataProperties.h
//  Learning App
//
//  Created by HPL on 15/12/16.
//  Copyright © 2016 heypayless. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserData.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserData (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *lang_id;
@property (nullable, nonatomic, retain) NSString *level_id;
@property (nullable, nonatomic, retain) NSString *medal;
@property (nullable, nonatomic, retain) NSString *quiz_no;
@property (nullable, nonatomic, retain) NSString *star;

@end

NS_ASSUME_NONNULL_END
