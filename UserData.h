//
//  UserData.h
//  Learning App
//
//  Created by HPL on 25/10/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserData : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "UserData+CoreDataProperties.h"
