//
//  AlphabetsViewController.swift
//  Learning App
//
//  Created by HPL on 11/08/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import SDWebImage
import SWRevealViewController

class AlphabetsViewController: UIViewController,UITableViewDataSource, UITableViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegate, AVAudioPlayerDelegate, UITextFieldDelegate {

    @IBOutlet var alphabetDataCollectionView: UICollectionView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet weak var btnRightBarButton: UIBarButtonItem!
    var playerForAudio:AVAudioPlayer = AVAudioPlayer()
    var playerOrg:AVAudioPlayer = AVAudioPlayer()

    var languageData : NSMutableArray = NSMutableArray()
    var appdelegate: AppDelegate?
    var selectedIndex : NSIndexPath?
    var languageCode = String()
    var quizCount : Int?
    var AllData : NSDictionary?
    var AllDataSelected : NSDictionary?
    var scrollViewWidth : CGFloat = 0
    var searchTearm : String?
    var isSearchActive = false
    var searchResults : NSArray?
    var PrefereFirstLanguage : String = ""
    var isLangChanged = false
     var test : String?
    var count = 0
    //var searchedArray : NSMutableArray = NSMutableArray()
//    let tableList = ["Apple","Apricot","Aeroplane","Alpha","Alphabet","Banana","Ball","Cat","Dog","Doggie","Pussy","Doll","Carrot","Elephant"]
    var filteredList = [AlphabetsTableViewCell]()
    let tap = UITapGestureRecognizer()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.txtSearch.delegate = self
        let userdefault = NSUserDefaults.standardUserDefaults()
       self.languageCode = userdefault.objectForKey("lang_id") as! String
        self.quizCount = userdefault.objectForKey("quiz_no")! as? Int
        self.PrefereFirstLanguage = userdefault.objectForKey("Language") as! String
        print(userdefault.objectForKey("lang_id") as! String)
        print(userdefault.objectForKey("quiz_no") as! String)
        // Do any additional setup after loading the view.
        
        btnRightBarButton.target = self.revealViewController()
        btnRightBarButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationItem.title = "Star Catchers"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.whiteColor(),
             NSFontAttributeName: UIFont(name: "HVDComicSerifPro", size: 21)!]
        //self.navigationItem.leftBarButtonItem?.title = "back"
        //self.navigationItem.backBarButtonItem?.title = "back"
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "back", style: .Plain, target: nil, action: nil)
        
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "NewLangId")
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .Plain, target: nil, action: nil)
        
        txtSearch.layer.cornerRadius = txtSearch.frame.height/2
        txtSearch.layer.masksToBounds = true
        txtSearch.layer.borderWidth = 2
        txtSearch.layer.borderColor = UIColor.whiteColor().CGColor

        tableView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
         //self.player.delegate = self
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(
            self,selector: #selector(AlphabetsViewController.textFieldTextChanged(_:)),name:UITextFieldTextDidChangeNotification,object: nil
        )
        
        
        alphabetDataCollectionView.dataSource = self
        alphabetDataCollectionView.delegate = self
        
         appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        
    }
    
    func endEditing(sender: UITapGestureRecognizer) {
        self.view.removeGestureRecognizer(tap)
        self.view.endEditing(true)
    }
//
//    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
//        textField.resignFirstResponder()
//        return true
//    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(true)
        //let scrollIndex = NSIndexPath(forRow: 0, inSection: 0)
        //tableView.reloadRowsAtIndexPaths([scrollIndex], withRowAnimation: UITableViewRowAnimation.None)
        self.navigationController!.setNavigationBarHidden(false, animated: false)
      //  print(NSUserDefaults.standardUserDefaults().stringForKey("NewLangId"))
       // testForEmptyData()
        let userdefault = NSUserDefaults.standardUserDefaults()
        self.languageCode = userdefault.objectForKey("lang_id")! as! String
     print(userdefault.objectForKey("lang_id") as! String)
        self.quizCount = userdefault.objectForKey("quiz_no")! as? Int
        print(quizCount)
        self.PrefereFirstLanguage = userdefault.objectForKey("Language") as! String
        print(PrefereFirstLanguage)
        self.getDetailLanguagesDataFromWebService(languageCode,alpahabet: "A")
       
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        alphabetDataCollectionView.allowsMultipleSelection = false
         alphabetDataCollectionView.allowsSelection = true
    }
    
    func testForEmptyData()
    {
        test = NSUserDefaults.standardUserDefaults().stringForKey("NewLangId")
        if(test == nil)
        {
            
        }
        else{
         self.getDetailLanguagesDataFromWebService(test!,alpahabet: "A")
            isLangChanged = true
        }
    }

    
    func textFieldTextChanged(sender : AnyObject)
    {
        if(txtSearch.text!.isEmpty)
        {
            self.tableView.reloadData()
            self.txtSearch.tintColor = UIColor.clearColor()
            
        }
        else{
        isSearchActive = true
        self.txtSearch.tintColor = UIColor.blackColor()
        let searchPredicate = NSPredicate(format: "word_name beginswith[c] %@", txtSearch.text!)
        searchTearm = txtSearch.text!
        let array = (self.languageData as NSArray).filteredArrayUsingPredicate(searchPredicate)
        self.searchResults = array
        print(searchResults)
        self.disablesAutomaticKeyboardDismissal()
        self.tableView.reloadData()

        }
    }
    
    @IBAction func searchButtonClicked(sender: AnyObject)
    {
        if(txtSearch.text!.isEmpty)
        {
            
        }
        else
        {
        isSearchActive = true
        searchTearm = txtSearch.text!
        let searchPredicate = NSPredicate(format: "word_name beginswith[c] %@", txtSearch.text!)
         searchTearm = txtSearch.text!
        let array = (self.languageData as NSArray).filteredArrayUsingPredicate(searchPredicate)
        self.searchResults = array
        print(searchResults)
        self.tableView.reloadData()
        }
    }
    
    func getDetailLanguagesDataFromWebService(languageId : String  , alpahabet : String)
    {
        appdelegate!.showHUD(self.view)
        print(languageId)
        if Reachability.isConnectedToNetwork() == true
        {
            Alamofire.request(.POST, "http://languagestar.net/admin/public/word-data", parameters: ["language_id": languageId,"alpha":alpahabet])
                .responseJSON { _, _, result in
                    //                print(result.request)  // original URL request
                    //                print(result.response) // URL response
                    //                print(result.data)     // server data
                    //                print(result.result)   // result of response serialization
                    
                    if let JSON = result.value
                    {
                        print("JSON: \(JSON)")
                        self.appdelegate?.dismissView(self.view)
                        if(JSON.objectForKey("status")!.isEqualToString("fail"))
                        {
                            let Cust = CustomClass()
                            var alert = UIAlertController( )
                            alert = Cust.displayMyAlertMessage("Words are not available for alphabet \(alpahabet)")
                            self.presentViewController(alert, animated: true, completion: nil)
                            self.languageData = []
                            dispatch_async(dispatch_get_main_queue(),
                                {
                                    self.tableView.reloadData()
                            })
                            
                        }
                        else{
                            
                            self.languageData.setArray(JSON.objectForKey("data")! as! [AnyObject])
                            print("JSON: \(self.languageData)")
                            
                            dispatch_async(dispatch_get_main_queue(),
                                {
                                    self.tableView.reloadData()
                            })
                        }
                    }
                    else
                    {
                        self.appdelegate?.dismissView(self.view)
                        let Cust = CustomClass()
                        var alert = UIAlertController( )
                        alert = Cust.displayMyAlertMessage("Words are not available for alphabet \(alpahabet)")
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
            }
        }else{
            self.appdelegate?.dismissView(self.view)
            let Cust = CustomClass()
            var alert = UIAlertController( )
            alert = Cust.displayMyAlertMessage("Please check your internet connection.")
            self.presentViewController(alert, animated: true, completion: nil)
        }

    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 26
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageCollectionViewCell", forIndexPath: indexPath) as! ImageCollectionViewCell
        cell.imgAlphabet.image = UIImage(named: "Alphabet_\(Character(UnicodeScalar(65+indexPath.item)))")
        cell.lblAlphabet.text = "\(Character(UnicodeScalar(65+indexPath.item))) \(Character(UnicodeScalar(97+indexPath.item)))"
        if UIScreen.mainScreen().bounds.size.height == 480 {
            // iPhone 4
            cell.lblAlphabet.font = cell.lblAlphabet.font.fontWithSize(30)
        } else if UIScreen.mainScreen().bounds.size.height == 568 {
            // IPhone 5
            cell.lblAlphabet.font = cell.lblAlphabet.font.fontWithSize(42)
        } else if UIScreen.mainScreen().bounds.size.width == 375 {
            // iPhone 6
            cell.lblAlphabet.font = cell.lblAlphabet.font.fontWithSize(50)
        } else if UIScreen.mainScreen().bounds.size.width == 414 {
            // iPhone 6+
            cell.lblAlphabet.font = cell.lblAlphabet.font.fontWithSize(60)
        } else if UIScreen.mainScreen().bounds.size.width == 768 {
            // iPad
            cell.lblAlphabet.font = cell.lblAlphabet.font.fontWithSize(80)
        }
        
        cell.imgAlphabet.layer.cornerRadius = 5
        cell.imgAlphabet.clipsToBounds = true
        
        if(cell.lblAlphabet.textColor == UIColor.whiteColor())
        {
            cell.lblAlphabet.textColor = UIColor.lightGrayColor()
        }
        
        if(indexPath == selectedIndex)
        {
            cell.lblAlphabet.textColor = UIColor.whiteColor()
        }
        
        if(indexPath.row == 0 && cell.tag == 0)
        {
            selectedIndex = indexPath
            cell.lblAlphabet.textColor = UIColor.whiteColor()
        }
        
        cell.tag = 1
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: self.view.frame.height*0.15*1.5, height: self.view.frame.height*0.15)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        let DidSelectcell = collectionView.cellForItemAtIndexPath(indexPath) as! ImageCollectionViewCell
        DidSelectcell.lblAlphabet.textColor = UIColor.whiteColor()
        
        if(selectedIndex != indexPath)
        {
            if let DidDeselectcell = collectionView.cellForItemAtIndexPath(selectedIndex!) as? ImageCollectionViewCell
            {
                DidDeselectcell.lblAlphabet.textColor = UIColor.lightGrayColor()
            }
          print((Character(UnicodeScalar(65+indexPath.item))))
            let alpha = String(Character(UnicodeScalar(65+indexPath.item)))
            print(alpha)
            //Call reload API
            txtSearch.text = ""
            isSearchActive = false
            if(isLangChanged == false){
            self.getDetailLanguagesDataFromWebService(languageCode,alpahabet: alpha)
            }
            else{
                self.getDetailLanguagesDataFromWebService(test!,alpahabet: alpha)
            }
            //tableView.reloadRowsAtIndexPaths([scrollIndex], withRowAnimation: UITableViewRowAnimation.None)
            self.tableView.reloadData()
            if languageData.count != 0 {
                let scrollIndex = NSIndexPath(forRow: 0, inSection: 0)
                self.tableView.scrollToRowAtIndexPath(scrollIndex, atScrollPosition: UITableViewScrollPosition.Top, animated: false)
            }
        }
    
        self.selectedIndex = indexPath
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        print("Deselect")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (isSearchActive == false)
        {
        return self.languageData.count
        }
        else
        {
            if(txtSearch.text!.isEmpty){
                return self.languageData.count
                
            }
            else
            {
            print(searchResults!.count)
           return searchResults!.count
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
       
        var CommonUrl = "http://languagestar.net/admin/public/assets/image/"
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Word", forIndexPath: indexPath) as! AlphabetsTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
//        let indexPath1 = NSIndexPath(forRow: 0, inSection: 0)
//        tableView.scrollToRowAtIndexPath(indexPath1, atScrollPosition: .None, animated: false)
        if (isSearchActive == false) {
        AllData = self.languageData[indexPath.row] as? NSDictionary
        print(AllData)
        cell.lblEnglishWord.text = AllData?.objectForKey("word_name") as? String
        cell.lblWordPronunciation.text = AllData?.objectForKey("native_word_name") as? String
            let capsString = AllData?.objectForKey("word_name") as? String
            let capsResult = capsString?.capitalizedString
            cell.lblEnglishWord.text = capsResult
            
            let wrdString = AllData?.objectForKey("native_word_name") as? String
            let wrdResult = wrdString?.capitalizedString
            cell.lblWordPronunciation.text = wrdResult

        CommonUrl += (AllData?.objectForKey("image_name") as? String)!
        print(CommonUrl)
        //let url = NSURL(string: CommonUrl)!
        let block = {(image: UIImage!, error: NSError!, cacheType:SDImageCacheType, imageURL: NSURL!) -> Void in
            print(self)
        }
        cell.imgWord.sd_setImageWithURL(NSURL(string: CommonUrl), completed: block)
        cell.PlayButtonTapped.accessibilityIdentifier = String(indexPath.row)
        cell.PlayButtonTapped.addTarget(self, action: #selector(AlphabetsViewController.yourButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            
        }
        else
        {
            if(txtSearch.text!.isEmpty)
                {
                    AllData = self.languageData[indexPath.row] as? NSDictionary
                    print(AllData)
                    cell.lblEnglishWord.text = AllData?.objectForKey("word_name") as? String
                    cell.lblWordPronunciation.text = AllData?.objectForKey("native_word_name") as? String
                    CommonUrl += (AllData?.objectForKey("image_name") as? String)!
                    print(CommonUrl)
                    //let url = NSURL(string: CommonUrl)!
                    let block = {(image: UIImage!, error: NSError!, cacheType:SDImageCacheType, imageURL: NSURL!) -> Void in
                        print(self)
                    }
                    cell.imgWord.sd_setImageWithURL(NSURL(string: CommonUrl), completed: block)
                    cell.PlayButtonTapped.accessibilityIdentifier = String(indexPath.row)
                    cell.PlayButtonTapped.addTarget(self, action: #selector(AlphabetsViewController.yourButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)

                }
            else
            {
            //searchResults
            cell.lblEnglishWord.text = searchResults?.objectAtIndex(indexPath.row).objectForKey("word_name") as? String
            cell.lblWordPronunciation.text = searchResults?.objectAtIndex(indexPath.row).objectForKey("native_word_name") as? String
            CommonUrl += (searchResults?.objectAtIndex(indexPath.row).objectForKey("image_name") as? String)!
            print(CommonUrl)
            //let url = NSURL(string: CommonUrl)!
            let block = {(image: UIImage!, error: NSError!, cacheType:SDImageCacheType, imageURL: NSURL!) -> Void in
                print(self)
            }
            cell.imgWord.sd_setImageWithURL(NSURL(string: CommonUrl), completed: block)
            cell.PlayButtonTapped.accessibilityIdentifier = String(indexPath.row)
            cell.PlayButtonTapped.addTarget(self, action: #selector(AlphabetsViewController.yourButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)

            }
        }
        
        return cell
    }
    
    func yourButtonClicked(sender : UIButton)
    {
        if(isSearchActive == false)
        {
        let buttonRow = Int(sender.accessibilityIdentifier!)
        print(buttonRow)
        var commonUrlForAudio = "http://languagestar.net/admin/public/assets/audio/"
        //var commonUrlForOriginalAudio = "http://172.16.2.17/dictionary/public/assets/audio/"
        AllDataSelected = self.languageData[buttonRow!] as? NSDictionary
        print(AllDataSelected)
        commonUrlForAudio += (AllDataSelected!.objectForKey("audio_name") as? String)!
        //commonUrlForOriginalAudio += (AllDataSelected!.objectForKey("original_audio_name") as? String)!
        print(commonUrlForAudio)
        let url = NSURL(string: commonUrlForAudio)
        //let urlOrig = NSURL(string: commonUrlForOriginalAudio)
        downloadFileFromURL(url!)
       // downloadFileFromURLOrg(urlOrig!)
        }
        else
        {
            let buttonRow = Int(sender.accessibilityIdentifier!)
            print(buttonRow)
            var commonUrlForAudio = "http://languagestar.net/admin/public/assets/audio/"
           // var commonUrlForOriginalAudio = "http://172.16.2.17/dictionary/public/assets/audio/"
            AllDataSelected = self.searchResults![buttonRow!] as? NSDictionary
            print(AllDataSelected)
            commonUrlForAudio += (AllDataSelected!.objectForKey("audio_name") as? String)!
            //commonUrlForOriginalAudio += (AllDataSelected!.objectForKey("original_audio_name") as? String)!
            print(commonUrlForAudio)
            let url = NSURL(string: commonUrlForAudio)
            //let urlOrig = NSURL(string: commonUrlForOriginalAudio)
            downloadFileFromURL(url!)
            //downloadFileFromURL(urlOrig!)

        }
        
    }
    
 // Alamofire URL call method
        
//        var lastUrl = "1471426934.mp3"
//        let LastUrlFinal = NSURL(string: lastUrl)
////        Alamofire.download(.GET, url!) { temporaryURL, response in
////            let fileManager = NSFileManager.defaultManager()
////            let directoryURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
////            let pathComponent = response.suggestedFilename
////            
////           
////            print(directoryURL.URLByAppendingPathComponent(pathComponent!))
////            self.play(directoryURL.URLByAppendingPathComponent(pathComponent!))
////            return directoryURL.URLByAppendingPathComponent(pathComponent!)
////        }
//        if let directoryURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as? NSURL
//        {
//            var localImageURL = directoryURL.
//            return localImageURL
//        }
//         let url1 = NSURL(string: DestinationFilePath)
//        let destination = Alamofire.Request.suggestedDownloadDestination(directory: .DocumentDirectory, domain: .UserDomainMask)
//        Alamofire.download(.GET, url!, destination: destination)
//            .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
//                print(totalBytesRead)
//                print(bytesRead)
//                print(totalBytesExpectedToRead)
//              
//                
//                
//              
//                
//                
//               
//                self.play(url1!)
//               
//                // This closure is NOT called on the main queue for performance
//                // reasons. To update your ui, dispatch to the main queue.
//                dispatch_async(dispatch_get_main_queue()) {
//                    print("Total bytes read on main queue: \(totalBytesRead)")
//                }
//            }
//            .response { _, _, _, error in
//                if let error = error
//                {
//                    print("Failed with error: \(error)")
//                    self.play(url1!)
//                    
//                }
//                else
//                {
//                    print("Downloaded file successfully")
//                  
//                }
//        }
//
//    }
        
        
        
        
    func downloadFileFromURL(url:NSURL){
        var downloadTask:NSURLSessionDownloadTask
        downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(url, completionHandler: { (URL, response, error) -> Void in
            //self.count += 1
            if error != nil {
                print("There was some problem with the Sound")
            } else {
                print(URL!)
                self.play(URL!)
            }
        })
        
        downloadTask.resume()
    }
    func play(url:NSURL) {
        print("playing \(url)")
        do {
            self.playerForAudio = try AVAudioPlayer(contentsOfURL: url)
            playerForAudio.delegate = self
            playerForAudio.prepareToPlay()
            playerForAudio.volume = 1.0
             //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(playerDidFinishPlaying(_:)), name: AVPlayerItemDidPlayToEndTimeNotification, object:self.playerForAudio)
            playerForAudio.play()
            
        } catch let error as NSError {
            //self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool){
        print(count)
        if(count == 0){
                    var commonUrlForOriginalAudio = "http://languagestar.net/admin/public/assets/audio/"
                    commonUrlForOriginalAudio += (self.AllDataSelected!.objectForKey("original_audio_name") as? String)!
                    let urlOrig = NSURL(string: commonUrlForOriginalAudio)
                    self.downloadFileFromURL(urlOrig!)
                     count += 1
                    }
        else {
            count = 0
        }

    }
    
//     func playerDidFinishPlaying(note: NSNotification){
//    if(count == 1){
//        var commonUrlForOriginalAudio = "http://172.16.2.17/dictionary/public/assets/audio/"
//        commonUrlForOriginalAudio += (self.AllDataSelected!.objectForKey("original_audio_name") as? String)!
//        let urlOrig = NSURL(string: commonUrlForOriginalAudio)
//        self.downloadFileFromURL(urlOrig!)
//         count = 0
//        }
//
//    }
//    
//    
//    func downloadFileFromURLOrg(url:NSURL){
//        var downloadTask:NSURLSessionDownloadTask
//        downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(url, completionHandler: { (URL, response, error) -> Void in
//            self.count += 1
//            self.playOrg(URL!)
//            
//          })
//        
//        downloadTask.resume()
//    }
//    func playOrg(url:NSURL) {
//        print("playing \(url)")
//        do {
//            self.playerOrg = try AVAudioPlayer(contentsOfURL: url)
//            playerOrg.prepareToPlay()
//            playerOrg.volume = 1.0
//            playerOrg.play()
//           NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AlphabetsViewController.playerDidFinishPlaying(_:)), name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
//        } catch let error as NSError {
//            //self.player = nil
//            print(error.localizedDescription)
//        } catch {
//            print("AVAudioPlayer init failed")
//        }
//        
//    }
//
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    @IBAction func btnSettingsAction(sender: AnyObject)
    {
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        tap.addTarget(self, action: #selector(endEditing))
        self.view.addGestureRecognizer(tap)
    }
    
//    func textFieldDidEndEditing(textField: UITextField)
//    {
//        
//    }
    
}




