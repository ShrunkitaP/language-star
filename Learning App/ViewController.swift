//
//  ViewController.swift
//  Learning App
//
//  Created by HPL on 11/08/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit
import Alamofire
import SWRevealViewController

class ViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate
{
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var imgBackground: UIImageView!
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var btnSelectLanguage: UIButton!
   // var languageIden : Int?
    var btnState : Bool = false
    var appdelegate: AppDelegate?
    var selectedRow : Int?
    var languagePrefered: String = ""
    var langCode:AnyObject = ""
    @IBOutlet var listView: UIView!
    var languageData : NSMutableArray = NSMutableArray()
    var finalLanguageData : NSMutableArray = NSMutableArray()
    //var display : NSMutableArray = NSMutableDictionary()
    var selectLang_id = UserDatabase()
    var test : String?
     var isLangChanged = false
    var checkLang : String?
    var checkquiz : String?
    var appDelegate = AppDelegate()
  
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        print(btnSelectLanguage.titleLabel)
        checkLang = NSUserDefaults.standardUserDefaults().objectForKey("nextScreenLanguage") as? String
        if(checkLang == nil){
           // if checkLang == ""{
            // NSUserDefaults.standardUserDefaults().setObject("1", forKey: "nextScreenLanguage")
              //  NSUserDefaults.standardUserDefaults().objectForKey("nextScreenLanguage") as? String
              //  }
        }
        else
        {
            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
            self.navigationController?.pushViewController(destination, animated: true)
            
        }
        self.navigationItem.title = "Star Catchers"
        self.lblHeader.text = "SELECT YOUR NATIVE\nLANGUAGE"
        btnSelectLanguage.layer.cornerRadius = 6
        btnSelectLanguage.clipsToBounds = true
     //   btnSelectLanguage.titleEdgeInsets = UIEdgeInsets.init(top: <#T##CGFloat#>, left: <#T##CGFloat#>, bottom: <#T##CGFloat#>, right: <#T##CGFloat#>)
        self.navigationItem.title = "Star Catchers"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.whiteColor(),
             NSFontAttributeName: UIFont(name: "HVDComicSerifPro", size: 21)!]
        
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        
        
     }

    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
//        var daArray = selectLang_id.fetchAllRecord()
//        print(daArray)
        self.navigationController?.navigationBarHidden = false
        self.webServiceCallforFetchingLanguageData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = true
    }
                                                                
    
    func webServiceCallforFetchingLanguageData()
    {
        appdelegate!.showHUD(self.view)
        if Reachability.isConnectedToNetwork() == true
        {
            Alamofire.request(.GET, "http://languagestar.net/admin/public/laguage-list")
                .responseJSON { _, _, result in
                    if let JSON = result.value
                    {
                        self.appdelegate?.dismissView(self.view)
                        self.languageData.setArray(JSON.objectForKey("data")! as! [AnyObject])
                        print("JSON: \(self.languageData)")
                        print(self.languageData)
                        self.pickLanguageData()
                        self.pickerView.dataSource  = self
                        self.pickerView.delegate = self
                    }else{
                        self.appdelegate?.dismissView(self.view)
                        let Cust = CustomClass()
                        var alert = UIAlertController( )
                        alert = Cust.displayMyAlertMessage("No data found.")
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
            }

        }else{
            self.appdelegate?.dismissView(self.view)
            let Cust = CustomClass()
            var alert = UIAlertController( )
            alert = Cust.displayMyAlertMessage("Please check your internet connection.")
            self.presentViewController(alert, animated: true, completion: nil)

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnSelectLanguageAction(sender: AnyObject)
    {
       
//         let userdefault = NSUserDefaults.standardUserDefaults()
//        self.languagePrefered = userdefault.objectForKey("language") as! String
         self.listView.hidden = false
    }
    
    func pickLanguageData()
    {
 
     //   sum = languageData.objectAtIndex(i).valueForKey("Count") as! Int
        for i in 0...languageData.count-1
        {
            if (languageData.objectAtIndex(i).valueForKey("Count") as! NSString != 0)
            {
                 finalLanguageData.addObject(languageData.objectAtIndex(i))
                 print(finalLanguageData)
            
            }
            else
            {
               // languageData.removeObject(i)
            }
        }
    }
    
     internal func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
     {
       return 1
     }
    
     func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
     {
        
             return self.finalLanguageData.count
     }
    internal func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
            return self.finalLanguageData.objectAtIndex(row).valueForKey("language") as? String
        
    }
    
    internal func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
          self.btnSelectLanguage.setTitle("  \(self.finalLanguageData.objectAtIndex(row).valueForKey("language") as! String)", forState: UIControlState.Normal)
    
        languagePrefered = (self.finalLanguageData.objectAtIndex(row).valueForKey("language") as? String)!
        print(languagePrefered)
        NSUserDefaults.standardUserDefaults().setObject(languagePrefered, forKey: "Language")
        
        langCode = self.finalLanguageData.objectAtIndex(row).objectForKey("id")!
        print(langCode)
        NSUserDefaults.standardUserDefaults().setObject(langCode, forKey: "lang_id")
        
        checkquiz = self.finalLanguageData.objectAtIndex(row).objectForKey("Count")! as? String
        print(checkquiz)
        NSUserDefaults.standardUserDefaults().setObject(checkquiz, forKey: "quiz_no")

        
//        self.btnSelectLanguage.setTitle(self.languageData.objectAtIndex(row).valueForKey("id") as? String, forState: UIControlState.Normal)
//        lang_id = (self.languageData.objectAtIndex(row).valueForKey("id") as? String)!
//        print(lang_id)
//        NSUserDefaults.standardUserDefaults().setObject(lang_id, forKey: "langId")

        self.listView.hidden = true
        selectedRow = row
//                performSegueWithIdentifier("Alphabets", sender: nil)
        //        if(btnState == false)
        //        {
        //            btnSelectLanguage.setBackgroundImage(UIImage(named: "Home_Unhidden"), forState: .Normal)
        //            btnState = true
        //        }
        //        else
        //        {
        //            btnSelectLanguage.setBackgroundImage(UIImage(named: "Home_Hidden"), forState: .Normal)
        //            btnState = false
        //        }
//        self.selectLang_id.saveName(String(languagePrefered))
    }
    


   
    @IBAction func submit(sender: UIButton) {
     langCode = self.finalLanguageData.objectAtIndex(self.selectedRow!).objectForKey("id")!
        checkquiz = self.finalLanguageData.objectAtIndex(self.selectedRow!).objectForKey("Count") as? String
        print(checkquiz)
        languagePrefered = self.finalLanguageData.objectAtIndex(self.selectedRow!).objectForKey("language") as! String
        print(languagePrefered)
        NSUserDefaults.standardUserDefaults().setObject("1", forKey: "nextScreenLanguage")
//        var checkquiz : Int?
//        checkquiz = languageData[selectedRow!].objectForKey("Count") as? Int
//        print(checkquiz)
//        if (checkquiz >= 48){
//            let userdefaults = NSUserDefaults.standardUserDefaults()
//            userdefaults.setValue(String(langCode), forKey: "LanguageCode")
//            userdefaults.setValue(checkquiz, forKey: "Count")
//            userdefaults.synchronize()
//            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
//            self.navigationController?.pushViewController(destination, animated: true)

//        }
//        else
//        {
//            let Cust = CustomClass()
//            var alert = UIAlertController( )
//            alert = Cust.displayMyAlertMessage("Quiz not available for \(languagePrefered) Language")
//            self.presentViewController(alert, animated: true, completion: nil)
//
//        }
        let userdefaults = NSUserDefaults.standardUserDefaults()
        userdefaults.setValue(String(langCode), forKey: "lang_id")
        userdefaults.setValue(checkquiz!, forKey: "quiz_no")
        userdefaults.setValue(String(languagePrefered), forKey: "language")
        userdefaults.synchronize()
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
}

