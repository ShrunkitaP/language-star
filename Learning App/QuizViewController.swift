//
//  QuizViewController.swift
//  Learning App
//
//  Created by HPL on 04/10/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit
import AVFoundation
import SDWebImage
//import MDCBlink.h

class QuizViewController: UIViewController,AVAudioPlayerDelegate  {
    
    var quizLevelArray : NSMutableArray = NSMutableArray()
    var quizIdArray : NSMutableArray = NSMutableArray()
    var databaseDictionary : NSDictionary = NSDictionary()
    var fetchStar : NSArray = NSArray()
    var fetchPopupData : NSArray = NSArray()
    @IBOutlet var timer_Count_lbl: UILabel!
    
    @IBOutlet var timer_hide: UIView!
    @IBOutlet var timer_view: UIView!
    @IBOutlet var ratingsLabel: UILabel!
    @IBOutlet var timerLabel: UILabel!
    @IBOutlet var timerImage: UIImageView!
    @IBOutlet var rightAnsLabel: UILabel!
    @IBOutlet var starOne: UIImageView!
    @IBOutlet var starTwo: UIImageView!
    @IBOutlet var starThree: UIImageView!
     
    @IBOutlet var cartoonImage: UIImageView!
    @IBOutlet var native_name: UILabel!
    @IBOutlet var buttonOne: UIButton!
    @IBOutlet var buttonOneImage: UIImageView!
    @IBOutlet var buttonOneLabel: UILabel!
    @IBOutlet var buttonTwo: UIButton!
    @IBOutlet var buttonTwoImage: UIImageView!
    @IBOutlet var buttonTwoLabel: UILabel!
    @IBOutlet var buttonThree: UIButton!
    @IBOutlet var buttonThreeImage: UIImageView!
    @IBOutlet var buttonThreeLabel: UILabel!
    @IBOutlet var buttonFour: UIButton!
    @IBOutlet var buttonFourImage: UIImageView!
    @IBOutlet var buttonFourLabel: UILabel!
   

    
    var name : String?
    var i = 0
    var counter = 16
    var url : String?
    var audioUrl : String?
    var audio_Url : String?
    var ansCounter = 0
    var selectedAns = String?()
    var item : Int?
    var starCount : Int?
    var levelId : String?
    var medal_quiz = ""
    var medal_quiz_check = "1"
    var langId : AnyObject = ""
    var quizId : String?
    var dataobject = UserDatabase()
    var blinkStatus = false
    var timerCount : NSTimer = NSTimer()
    let timeInterval:NSTimeInterval = 0.1
    let timerEnd:NSTimeInterval = 10.0
     // seconds or 2 hours
    var timeCount:Int = 960
    var timeCount1:Int = 840
    var timeCount3:Int = 720
      var alert = UIAlertController()
    var Cust = CustomClass()
    var blinkCount : NSTimeInterval = 60
    var user_store = [String:String]()
    var popup_store = [String:String]()
    var quizCount : Int?
    var fetch_total = Int()
    
    let commonUrl  = "http://languagestar.net/admin/public/assets/image/"
    var audioForPlay = "http://languagestar.net/admin/public/assets/audio/"
    
    var playerForAudio:AVAudioPlayer = AVAudioPlayer()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
         timerCount = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(QuizViewController.startTimer), userInfo: nil, repeats: true)
        langId = (NSUserDefaults.standardUserDefaults().objectForKey("lang_id")!)
        print(langId)
        quizCount = NSUserDefaults.standardUserDefaults().objectForKey("quiz_no") as? Int
        print(quizCount)
        print(levelId)
        if quizId == "1" || quizId == "4" || quizId == "7"{
           timer_hide.hidden = true
        }
        fetchPopupData = dataobject.fetchPopupData(String(starCount!), langKey: String(langId), levelKey: levelId!, quizKey: quizId!)
        print(fetchPopupData)
       // starCount = NSUserDefaults.standardUserDefaults().objectForKey("star") as? Int
        if fetchPopupData.count != 0{
        fetchPopupData = dataobject.fetchPopupData(String(starCount!), langKey: String(langId), levelKey: levelId!, quizKey: quizId!)
        print(fetchPopupData)
        for i in 0...fetchPopupData.count-1 {
            let popup_data = fetchPopupData.objectAtIndex(i)
            print(popup_data.valueForKey("lang_id")!)
            print(popup_data.valueForKey("level_id")!)
            print(popup_data.valueForKey("quiz_no")!)
            print(popup_data.valueForKey("star")! as! String)
             print(popup_data.valueForKey("medal")!)
            
            popup_store["lang_id"] = popup_data.valueForKey("lang_id") as? String
            popup_store["level_id"] = popup_data.valueForKey("level_id") as? String
            popup_store["quiz_no"] = String(popup_data.valueForKey("quiz_no")!)
            popup_store["star"] = popup_data.valueForKey("star") as? String
            popup_store["medal"] = popup_data.valueForKey("medal") as? String
            let fetch_stars_popup = popup_store["star"]
            print(fetch_stars_popup)
            let fetch_quiz_popup = popup_store["quiz_no"]
            print(fetch_quiz_popup)
            let fetch_medal_popup = popup_store["medal"]
            print(fetch_medal_popup)

            if fetch_quiz_popup == "1"{
                if fetch_stars_popup == "3"{
                  fetch_total = Int(fetch_stars_popup!)!
                    print(fetch_total)
                }
            }else if fetch_quiz_popup == "2"{
                if fetch_stars_popup == "3"{
                    fetch_total = Int(fetch_stars_popup!)!+3
                    print(fetch_total)

                }
                
            }else if fetch_quiz_popup == "3"{
                if fetch_stars_popup == "3"{
                    fetch_total = Int(fetch_stars_popup!)!+6
                    print(fetch_total)

                }
                
            }
            else if fetch_quiz_popup == "4"{
                if fetch_stars_popup == "3"{
                    fetch_total = Int(fetch_stars_popup!)!
                    print(fetch_total)
                }
                
            }
            else if fetch_quiz_popup == "5"{
                if fetch_stars_popup == "3"{
                    fetch_total = Int(fetch_stars_popup!)!+3
                    print(fetch_total)

                }
                
            }
            else if fetch_quiz_popup == "6"{
                if fetch_stars_popup == "3"{
                    fetch_total = Int(fetch_stars_popup!)!+6
                    print(fetch_total)
                }

                
            }else if fetch_quiz_popup == "7"{
                if fetch_stars_popup == "3"{
                    fetch_total = Int(fetch_stars_popup!)!
                    print(fetch_total)
                }

            }
            else if fetch_quiz_popup == "8"{
                if fetch_stars_popup == "3"{
                    fetch_total = Int(fetch_stars_popup!)!+3
                    print(fetch_total)
                }
                
            }
            else if fetch_quiz_popup == "9"{
                if fetch_stars_popup == "3"{
                    fetch_total = Int(fetch_stars_popup!)!+6
                    print(fetch_total)
                }
            }
            print(fetch_total)
        }
        }else{
            self.navigationItem.hidesBackButton = true
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.quizStart()
        }
        self.navigationItem.hidesBackButton = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
      self.quizStart()
    }
    
    func startTimer()
    {
        self.timer_Count_lbl.textColor = UIColor.redColor()
        let blinkMinutes = UInt8(blinkCount / 60)
        let blinkSeconds = UInt8(blinkCount % 60)
        let Blink_final = String(format:"%02d:%02d", blinkMinutes, blinkSeconds )
        if levelId == "0" && quizId == "2" || quizId == "3"
        {
            print(timeCount)
            let minutes = UInt8(timeCount / 60)
            let sec = UInt8(timeCount%60)
            //            print(minutes)
            self.timer_Count_lbl.text = String(format:"%02d:%02d", minutes, sec)
            //            print(self.timer_Count_lbl.text)
            if self.timer_Count_lbl.text! == Blink_final
            {
                let timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(QuizViewController.blink), userInfo: nil, repeats: true)
            }
            if self.timeCount == 0{
                self.timerCount.invalidate()
                self.navigationController?.popViewControllerAnimated(false)
            }
            else
            {
                self.timeCount -= 1
            }
            
        }
        else if levelId == "1" && quizId == "5" || quizId == "6"{
            let blinkMinutes = UInt8(blinkCount / 60)
            let blinkSeconds = UInt8(blinkCount % 60)
            let Blink_final = String(format:"%02d:%02d", blinkMinutes, blinkSeconds )
            let minutes = UInt8(timeCount1 / 60)
            let sec = UInt8(timeCount1%60)
            self.timer_Count_lbl.text = String(format:"%02d:%02d", minutes, sec)
            if self.timer_Count_lbl.text! == Blink_final
            {
                let timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(QuizViewController.blink), userInfo: nil, repeats: true)
            }
            if self.timeCount1 == 0{
                self.timerCount.invalidate()
                self.navigationController?.popViewControllerAnimated(false)
            }
            else
            {
                self.timeCount1 -= 1
            }
            
            
        }
        else if levelId == "2" && quizId == "8" || quizId == "9"{
            let blinkMinutes = UInt8(blinkCount / 60)
            let blinkSeconds = UInt8(blinkCount % 60)
            let Blink_final = String(format:"%02d:%02d", blinkMinutes, blinkSeconds )
            let minutes = UInt8(timeCount3 / 60)
            let sec = UInt8(timeCount3%60)
            self.timer_Count_lbl.text = String(format:"%02d:%02d", minutes, sec)
            if self.timer_Count_lbl.text! == Blink_final
            {
                let timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(QuizViewController.blink), userInfo: nil, repeats: true)
            }
            if self.timeCount3 == 0{
                self.timerCount.invalidate()
                self.navigationController?.popViewControllerAnimated(false)
            }
            else
            {
                self.timeCount3 -= 1
            }
        }
        
    }

    func blink()
    {
        if blinkStatus == false {
            timer_Count_lbl.textColor = UIColor.grayColor()
            blinkStatus = true
        }
        else {
            timer_Count_lbl.textColor = UIColor.redColor()
            blinkStatus = false
        }
        
    }

    
    override func viewDidAppear(animated: Bool)
    {
              
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   func quizStart()
   {
    buttonOne.userInteractionEnabled = true
    buttonTwo.userInteractionEnabled = true
    buttonThree.userInteractionEnabled = true
    buttonFour.userInteractionEnabled = true
    
    print(quizLevelArray)
    
    buttonOne.backgroundColor = UIColor.clearColor()
    buttonTwo.backgroundColor = UIColor.clearColor()
    buttonThree.backgroundColor = UIColor.clearColor()
    buttonFour.backgroundColor = UIColor.clearColor()
    buttonOneLabel.textColor = UIColor.blackColor()
    buttonTwoLabel.textColor = UIColor.blackColor()
    buttonThreeLabel.textColor = UIColor.blackColor()
    buttonFourLabel.textColor = UIColor.blackColor()
    buttonOneImage.image = UIImage(named: "option_grey_bg.png")
    buttonTwoImage.image = UIImage(named: "option_grey_bg.png")
    buttonThreeImage.image = UIImage(named: "option_grey_bg.png")
    buttonFourImage.image = UIImage(named: "option_grey_bg.png")
    
    let option : NSMutableArray = NSMutableArray()
    
      // print(i)
    
   //    print(self.quizLevelArray)
    print(self.quizLevelArray.count)
    option.setArray(self.quizLevelArray.objectAtIndex(i).valueForKey("option") as! [AnyObject])
//     print(option)
    let caps = quizLevelArray.objectAtIndex(i).objectForKey("name") as? String
   // name = quizLevelArray.objectAtIndex(i).objectForKey("name") as? String
//    print(name!)
    name = caps?.capitalizedString
    let capitalnative = quizLevelArray.objectAtIndex(i).objectForKey("native_name") as? String
    native_name.text = capitalnative?.capitalizedString
//    print(native_name)
    
    let img_url =  quizLevelArray.objectAtIndex(i).objectForKey("image") as? String
    let block = {(image: UIImage!, error: NSError!, cacheType:SDImageCacheType, imageURL: NSURL!) -> Void in
        print(self)
    }
//    print("\(commonUrl)\(img_url!)")
    self.cartoonImage.sd_setImageWithURL(NSURL(string: "\(commonUrl)\(img_url!)"), completed: block)
    
    audio_Url = quizLevelArray.objectAtIndex(i).objectForKey("audio") as? String
    print(audio_Url!)
   // if quizLevelArray.count == 16{ } else{}
    for item in 0...option.count-1{
        option.setArray(self.quizLevelArray.objectAtIndex(i).valueForKey("option") as! [AnyObject])
        
//        buttonOne.setTitle("\(option[0])" , forState: UIControlState.Normal)
        buttonOneLabel.text = "\(option[0])".capitalizedString
//        buttonTwo.setTitle("\(option[1])" , forState: UIControlState.Normal)
        buttonTwoLabel.text = "\(option[1])".capitalizedString
//        buttonThree.setTitle("\(option[2])" , forState: UIControlState.Normal)
        buttonThreeLabel.text = "\(option[2])".capitalizedString
//        buttonFour.setTitle("\(option[3])" , forState: UIControlState.Normal)
        buttonFourLabel.text = "\(option[3])".capitalizedString
        
             switch(item) {
                
              case 0:
            buttonOne.tag = 0
          
            if (option.objectAtIndex(item).capitalizedString as NSString).isEqualToString(name!){
                buttonOne.tag = 1
            }
            else
            {
                buttonOne.tag = 0
            }
           break
        case 1 :
            buttonTwo.tag = 0
            if (option.objectAtIndex(item).capitalizedString as NSString).isEqualToString(name!){
                buttonTwo.tag = 1
            }
            else
            {
                buttonTwo.tag = 0
            }
            break
        case 2:
            buttonThree.tag = 0
           if (option.objectAtIndex(item).capitalizedString as NSString).isEqualToString(name!){
                buttonThree.tag = 1
            }
            else
            {
                buttonThree.tag = 0
            }
            break
        case  3:
            buttonFour.tag = 0
           if ( option.objectAtIndex(item).capitalizedString as NSString).isEqualToString(name!){
                buttonFour.tag = 1
            }
           else
            {
                buttonFour.tag = 0
            }
           break
        default:
            
            break
                
        }
     
    }
    
}
    
    @IBAction func audioButton(sender: UIButton) {
        
    let audioUrl = NSURL(string: "\(audioForPlay)\(audio_Url!)")
      print("\(audioForPlay)\(audio_Url!)")
        var downloadTask:NSURLSessionDownloadTask
        downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(audioUrl!, completionHandler: { (URL, response, error) -> Void in
            //self.count += 1
            if error != nil {
                print("playing sound failed")
            } else {
                self.play(URL!)
            }
            
        })
        
        downloadTask.resume()
    }
    
    
    func play(url:NSURL) {
        print("playing \(url)")
        do {
            self.playerForAudio = try AVAudioPlayer(contentsOfURL: url)
            playerForAudio.delegate = self
            playerForAudio.prepareToPlay()
            playerForAudio.volume = 1.0
            //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(playerDidFinishPlaying(_:)), name: AVPlayerItemDidPlayToEndTimeNotification, object:self.playerForAudio)
            playerForAudio.play()
            
        } catch let error as NSError {
            //self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
    }
    
  @IBAction func CheckButton(sender: UIButton) {
    
    
    let btn_temp = sender
    btn_temp.multipleTouchEnabled = true
    if btn_temp == self.buttonOne
    {
        self.buttonTwo.selected = false
        self.buttonThree.selected = false
        self.buttonFour.selected = false
        buttonOne.userInteractionEnabled = false
        buttonTwo.userInteractionEnabled = false
        buttonThree.userInteractionEnabled = false
        buttonFour.userInteractionEnabled = false

        if btn_temp.tag == 1
        {
            self.buttonOneImage.image = UIImage(named: "option_green_bg.png")
                        do {
                let urlright = NSBundle.mainBundle().URLForResource("rightAns", withExtension: "mp3")
                self.play(urlright!)
            }
            catch
            {
                fatalError("err")
            }

            if ansCounter < 17{
                self.ansCounter+=1
            }
            print(ansCounter)
            rightAnsLabel.text = (string: "\(ansCounter)\\\(counter)")
            

        }
        else
        {
            self.buttonOneImage.image = UIImage(named: "wrong_answer.png")
            do {
                let url = NSBundle.mainBundle().URLForResource("wrongAns", withExtension: "mp3")
                let duaPlayer = try AVAudioPlayer(contentsOfURL: url!)
                duaPlayer.prepareToPlay()
                print (url!)
                duaPlayer.volume = 1.0
                duaPlayer.play()
                var downloadTask:NSURLSessionDownloadTask
                downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(url!, completionHandler: { (URL, response, error) -> Void in
                    self.play(URL!)
                })
                
                downloadTask.resume()
            }
            catch
            {
                fatalError("err")
            }


        }
        
    }
    else if btn_temp == self.buttonTwo
    {
        self.buttonOne.selected = false
        self.buttonThree.selected = false
        self.buttonFour.selected = false
        buttonOne.userInteractionEnabled = false
         buttonTwo.userInteractionEnabled = false
        buttonThree.userInteractionEnabled = false
        buttonFour.userInteractionEnabled = false


        if btn_temp.tag == 1
        {
            self.buttonTwoImage.image = UIImage(named: "option_green_bg.png")
            
            do {
                let urlright = NSBundle.mainBundle().URLForResource("rightAns", withExtension: "mp3")
                self.play(urlright!)
            }
            catch
            {
                fatalError("err")
            }
            if ansCounter < 17{
                self.ansCounter+=1
            }
            print(ansCounter)
            rightAnsLabel.text = (string: "\(ansCounter)\\\(counter)")

        }
        else
        {
            self.buttonTwoImage.image = UIImage(named: "wrong_answer.png")
            do {
                let url = NSBundle.mainBundle().URLForResource("wrongAns", withExtension: "mp3")
                let duaPlayer = try AVAudioPlayer(contentsOfURL: url!)
                duaPlayer.prepareToPlay()
                print (url!)
                duaPlayer.volume = 1.0
                duaPlayer.play()
                var downloadTask:NSURLSessionDownloadTask
                downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(url!, completionHandler: { (URL, response, error) -> Void in
                    self.play(URL!)
                })
                
                downloadTask.resume()
            }
            catch
            {
                fatalError("err")
            }

        }
        
    }
    else if btn_temp == self.buttonThree
    {
        self.buttonTwo.selected = false
        self.buttonOne.selected = false
        self.buttonFour.selected = false
        buttonOne.userInteractionEnabled = false
        buttonTwo.userInteractionEnabled = false
        buttonThree.userInteractionEnabled = false
        buttonFour.userInteractionEnabled = false
        
        if btn_temp.tag == 1
        {
            self.buttonThreeImage.image = UIImage(named: "option_green_bg.png")

            do {
                let urlright = NSBundle.mainBundle().URLForResource("rightAns", withExtension: "mp3")
                self.play(urlright!)
            }
            catch
            {
                fatalError("err")
            }

            if ansCounter < 17{
                self.ansCounter+=1
            }
            print(ansCounter)
            rightAnsLabel.text = (string: "\(ansCounter)\\\(counter)")

        }
        else
        {
            self.buttonThreeImage.image = UIImage(named: "wrong_answer.png")
            do {
                let url = NSBundle.mainBundle().URLForResource("wrongAns", withExtension: "mp3")
                let duaPlayer = try AVAudioPlayer(contentsOfURL: url!)
                duaPlayer.prepareToPlay()
                print (url!)
                duaPlayer.volume = 1.0
                duaPlayer.play()
                var downloadTask:NSURLSessionDownloadTask
                downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(url!, completionHandler: { (URL, response, error) -> Void in
                    self.play(URL!)
                })
                
                downloadTask.resume()
            }
            catch
            {
                fatalError("err")
            }
        }
        
    }
    else if btn_temp == self.buttonFour
    {
        self.buttonTwo.selected = false
        self.buttonThree.selected = false
        self.buttonOne.selected = false
        buttonFour.userInteractionEnabled = false
        buttonThree.userInteractionEnabled = false
        buttonTwo.userInteractionEnabled = false
        buttonOne.userInteractionEnabled = false

        if btn_temp.tag == 1
        {
            self.buttonFourImage.image = UIImage(named: "option_green_bg.png")
            do {
                let urlright = NSBundle.mainBundle().URLForResource("rightAns", withExtension: "mp3")
                self.play(urlright!)
            }
            catch
            {
                fatalError("err")
            }
            if ansCounter < 17{
                self.ansCounter+=1
            }
            print(ansCounter)
            rightAnsLabel.text = (string: "\(ansCounter)\\\(counter)")

        }
        else
        {
            self.buttonFourImage.image = UIImage(named: "wrong_answer.png")
            do {
                let url = NSBundle.mainBundle().URLForResource("wrongAns", withExtension: "mp3")
                let duaPlayer = try AVAudioPlayer(contentsOfURL: url!)
                duaPlayer.prepareToPlay()
                print (url!)
                duaPlayer.volume = 1.0
                duaPlayer.play()
                var downloadTask:NSURLSessionDownloadTask
                downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(url!, completionHandler: { (URL, response, error) -> Void in
                    self.play(URL!)
                })
                
                downloadTask.resume()
            }
            catch
            {
                fatalError("err")
            }

        }
        
    }
    if ansCounter < 6{
        starOne.image = UIImage(named: "grey_small_star_1x")
        starTwo.image = UIImage(named: "grey_small_star_1x")
        starThree.image = UIImage(named: "grey_small_star_1x")
        starCount = 0
    }else if ansCounter == 6 && ansCounter <= 11
    {
        starOne.image = UIImage(named: "small_star_1x")
        starOne.alpha = 0.0
        UIView.animateWithDuration(1.0,  animations: {
            self.starOne.alpha = 2.0
            UIView.setAnimationRepeatCount(2.0)
            self.ansCountButton(sender)
            }, completion: nil)
        starTwo.image = UIImage(named: "grey_small_star_1x")
        starThree.image = UIImage(named: "grey_small_star_1x")
        starCount = 1
        print(starCount!)
    } else if ansCounter == 11 && ansCounter <= 15{
        starOne.image = UIImage(named: "small_star_1x")
        starTwo.image = UIImage(named: "small_star_1x")
        starTwo.alpha = 0.0
        UIButton.animateWithDuration(1.0,  animations: {
            self.starTwo.alpha = 2.0
            UIButton.setAnimationRepeatCount(2.0)
            }, completion: nil)
        starThree.image = UIImage(named: "grey_small_star_1x")
        starCount = 2
        print(starCount!)
    }else if  ansCounter == 16{
        starOne.image = UIImage(named: "small_star_1x")
        starTwo.image = UIImage(named: "small_star_1x")
        starThree.image = UIImage(named: "small_star_1x")
        starThree.alpha = 0.0
        UIButton.animateWithDuration(1.0,  animations: {
            self.starThree.alpha = 2.0
            UIButton.setAnimationRepeatCount(2.0)
            }, completion: nil)
        starCount = 3
        print(starCount!)
    }

    
    i += 1
    
    if i == counter {
        if sender.tag == 1
        {
                
            rightAnsLabel.text! = (string: "\(ansCounter)\\\(counter)")
            print(rightAnsLabel.text!)
            print(ansCounter)
        }
        
        sleep(1)
        print(starCount!)
        print(levelId!)
        print(String(langId))
        print(quizId!)
//        totalCountArray = (starCount)
        
        databaseDictionary = ["star": String(starCount!), "langKey":String(langId), "levelKey":levelId!, "quizKey":quizId! , "medal":medal_quiz]
//        print(databaseDictionary)
//        print(medal_quiz)
        fetchStar =  self.dataobject.fetchStardata(String(starCount!), langKey: String(langId), levelKey: levelId!, quizKey: quizId!, medal: medal_quiz)
        fetch_total = fetch_total + Int(starCount!)
        if fetchStar.count != 0
        {
        for i in 0...fetchStar.count-1  {
        let cord_data = fetchStar.objectAtIndex(i)
        print(cord_data.valueForKey("lang_id")!)
        print(cord_data.valueForKey("level_id")!)
        print(cord_data.valueForKey("quiz_no")!)
        print(cord_data.valueForKey("star")! as! String)
       
        user_store["lang_id"] = cord_data.valueForKey("lang_id") as? String
        user_store["level_id"] = cord_data.valueForKey("level_id") as? String
        user_store["quiz_no"] = String(cord_data.valueForKey("quiz_no")!)
        user_store["star"] = cord_data.valueForKey("star") as? String
            
              let fetch_stars_compare = user_store["star"]
            print(fetch_stars_compare)
            
            if (fetch_stars_compare < String(starCount!))
                
            {
                if fetch_total == 9{
                    if medal_quiz == ""{
                    medal_quiz = "1"
              //  self.dataobject.saveName(String(starCount!), langKey: String(langId), levelKey: levelId!, quizKey: quizId!, medalKey: medal_quiz!)
                self.dataobject.updateTable(quizId!, stars: String(starCount!), langid: String(langId), medal: medal_quiz)
                databaseDictionary = ["star": String(starCount!), "langKey":String(langId), "levelKey":levelId!, "quizKey":quizId! , "medal":medal_quiz]
                print(databaseDictionary)
                print(medal_quiz)
                    }
                }
                databaseDictionary = ["star": String(starCount!), "langKey":String(langId), "levelKey":levelId!, "quizKey":quizId! , "medal":medal_quiz]
                print(databaseDictionary)
                print(medal_quiz)
                self.dataobject.updateTable(quizId!, stars: String(starCount!), langid: String(langId), medal: medal_quiz)
         //       self.dataobject.updateTable(quizId!, stars: String(starCount!) , langid : String(langId) )
                print(self.navigationController?.viewControllers)
                let viewcontroller:DashboardScreenViewController = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-2] as! DashboardScreenViewController
                viewcontroller.starDashboardCount = starCount!
                viewcontroller.DataArrayDash = databaseDictionary
                viewcontroller.delegateFlag = true
                self.navigationController?.popViewControllerAnimated(false)
                
            }
            else
            {
                let viewcontroller:DashboardScreenViewController = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-2] as! DashboardScreenViewController
                viewcontroller.starDashboardCount = starCount!
                viewcontroller.DataArrayDash = databaseDictionary
                viewcontroller.delegateFlag = true
                self.navigationController?.popViewControllerAnimated(false)

            }
        }
        }else
        {
            if fetch_total == 9{
                if medal_quiz == ""{
                    medal_quiz = "1"
                    //self.dataobject.saveName(String(starCount!), langKey: String(langId), levelKey: levelId!, quizKey: quizId!, medalKey: medal_quiz!)
                    self.dataobject.updateTable(quizId!, stars: String(starCount!), langid: String(langId), medal: medal_quiz)
                    databaseDictionary = ["star": String(starCount!), "langKey":String(langId), "levelKey":levelId!, "quizKey":quizId! , "medal":medal_quiz]
                    print(databaseDictionary)
                    print(medal_quiz)
                }
            }

            self.dataobject.saveName(String(starCount!), langKey: String(langId), levelKey: levelId!, quizKey: quizId!, medalKey: medal_quiz)
            print(self.navigationController?.viewControllers)
            let viewcontroller:DashboardScreenViewController = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-2] as! DashboardScreenViewController
            viewcontroller.starDashboardCount = starCount!
            viewcontroller.DataArrayDash = databaseDictionary
            viewcontroller.medal_dash = medal_quiz
            viewcontroller.delegateFlag = true
            self.navigationController?.popViewControllerAnimated(false)

        }
    } else
    {
           sleep(1)
           quizStart()
    }
    }
    
    
    @IBAction func checkButton(sender: AnyObject) {
        
    let btn_temp = sender as! UIButton
        btn_temp.multipleTouchEnabled = true
        
        if btn_temp == self.buttonOne
        {
            self.buttonTwo.selected = false
            self.buttonThree.selected = false
            self.buttonFour.selected = false
            buttonOne.userInteractionEnabled = false
            buttonTwo.userInteractionEnabled = false
            buttonThree.userInteractionEnabled = false
            buttonFour.userInteractionEnabled = false


            if btn_temp.tag == 1
            {
               self.buttonOneImage.image = UIImage(named: "option_green_bg.png")
                self.buttonTwoImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonThreeImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonFourImage.image = UIImage(named: "option_grey_bg.png")
                do {
                    let url = NSBundle.mainBundle().URLForResource("rightAns", withExtension: "mp3")
                    let duaPlayer = try AVAudioPlayer(contentsOfURL: url!)
                    duaPlayer.prepareToPlay()
                    print (url!)
                    duaPlayer.volume = 1.0
                    duaPlayer.play()
                    var downloadTask:NSURLSessionDownloadTask
                    downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(url!, completionHandler: { (URL, response, error) -> Void in
                        self.play(URL!)
                    })
                    
                    downloadTask.resume()
                }
                catch
                {
                    fatalError("err")
                }

                
            }
            else
            {
                self.buttonOneImage.image = UIImage(named: "wrong_answer.png")
                self.buttonTwoImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonThreeImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonFourImage.image = UIImage(named: "option_grey_bg.png")
                
                do {
                    let url = NSBundle.mainBundle().URLForResource("rightAns", withExtension: "mp3")
                    let duaPlayer = try AVAudioPlayer(contentsOfURL: url!)
                    duaPlayer.prepareToPlay()
                    print (url!)
                    duaPlayer.volume = 1.0
                    duaPlayer.play()
                    var downloadTask:NSURLSessionDownloadTask
                    downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(url!, completionHandler: { (URL, response, error) -> Void in
                        self.play(URL!)
                    })
                    
                    downloadTask.resume()
                }
                catch
                {
                    fatalError("err")
                }

            }
            
        }
        else if btn_temp == self.buttonTwo
        {
            self.buttonOne.selected = false
            self.buttonThree.selected = false
            self.buttonFour.selected = false
            buttonOne.userInteractionEnabled = false
            buttonTwo.userInteractionEnabled = false
            buttonThree.userInteractionEnabled = false
            buttonFour.userInteractionEnabled = false

            if btn_temp.tag == 1
            {
                self.buttonTwoImage.image = UIImage(named: "option_green_bg.png")
                self.buttonOneImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonThreeImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonFourImage.image = UIImage(named: "option_grey_bg.png")
                 do {
                    let urlright = NSBundle.mainBundle().URLForResource("rightAns", withExtension: "mp3")
                    var duaPlayer = try AVAudioPlayer(contentsOfURL: urlright!)
                    duaPlayer.prepareToPlay()
                    print (urlright)
                    duaPlayer.volume = 1.0
                    duaPlayer.play()
                }
                catch
                {
                    fatalError("err")
                }

            }
            else
            {
                self.buttonTwoImage.image = UIImage(named: "wrong_answer.png")
                self.buttonOneImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonThreeImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonFourImage.image = UIImage(named: "option_grey_bg.png")
                do {
                    let url = NSBundle.mainBundle().URLForResource("wrongAns", withExtension: "mp3")
                    var duaPlayer = try AVAudioPlayer(contentsOfURL: url!)
                    duaPlayer.prepareToPlay()
                    print (url)
                    duaPlayer.volume = 1.0
                    duaPlayer.play()
                }
                catch
                {
                    fatalError("err")
                }
            }

        }
        else if btn_temp == self.buttonThree
        {
            self.buttonTwo.selected = false
            self.buttonOne.selected = false
            self.buttonFour.selected = false
            buttonOne.userInteractionEnabled = false
            buttonTwo.userInteractionEnabled = false
            buttonThree.userInteractionEnabled = false
            buttonFour.userInteractionEnabled = false

            if btn_temp.tag == 1
            {
                self.buttonThreeImage.image = UIImage(named: "option_green_bg.png")
                self.buttonOneImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonTwoImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonFourImage.image = UIImage(named: "option_grey_bg.png")
                                do {
                    let urlright = NSBundle.mainBundle().URLForResource("rightAns", withExtension: "mp3")
                    var duaPlayer = try AVAudioPlayer(contentsOfURL: urlright!)
                    duaPlayer.prepareToPlay()
                    print (urlright)
                    duaPlayer.volume = 1.0
                    duaPlayer.play()
                }
                catch
                {
                    fatalError("err")
                }

            }
            else
            {
                self.buttonThreeImage.image = UIImage(named: "wrong_answer.png")
                self.buttonOneImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonTwoImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonFourImage.image = UIImage(named: "option_grey_bg.png")
                do {
                    let url = NSBundle.mainBundle().URLForResource("wrongAns", withExtension: "mp3")
                    var duaPlayer = try AVAudioPlayer(contentsOfURL: url!)
                    duaPlayer.prepareToPlay()
                    print (url)
                    duaPlayer.volume = 1.0
                    duaPlayer.play()
                }
                catch
                {
                    fatalError("err")
                }

            }

        }
        else if btn_temp == self.buttonFour
        {
            self.buttonTwo.selected = false
            self.buttonThree.selected = false
            self.buttonOne.selected = false
            buttonFour.userInteractionEnabled = false
            buttonThree.userInteractionEnabled = false
            buttonTwo.userInteractionEnabled = false
            buttonOne.userInteractionEnabled = false

            if btn_temp.tag == 1
            {
                self.buttonFourImage.image = UIImage(named: "option_green_bg.png")
                self.buttonOneImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonTwoImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonThreeImage.image = UIImage(named: "option_grey_bg.png")
                do {
                    let urlright = NSBundle.mainBundle().URLForResource("rightAns", withExtension: "mp3")
                    var duaPlayer = try AVAudioPlayer(contentsOfURL: urlright!)
                    duaPlayer.prepareToPlay()
                    print (urlright)
                    duaPlayer.volume = 1.0
                    duaPlayer.play()
                }
                catch
                {
                    fatalError("err")
                }

            }
            else
            {
                self.buttonFourImage.image = UIImage(named: "wrong_answer.png")
                self.buttonOneImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonTwoImage.image = UIImage(named: "option_grey_bg.png")
                self.buttonThreeImage.image = UIImage(named: "option_grey_bg.png")

                do {
                    let url = NSBundle.mainBundle().URLForResource("wrongAns", withExtension: "mp3")
                    var duaPlayer = try AVAudioPlayer(contentsOfURL: url!)
                    duaPlayer.prepareToPlay()
                    print (url)
                    duaPlayer.volume = 1.0
                    duaPlayer.play()
                }
                catch
                {
                    fatalError("err")
                }

            }

        }
        
        if buttonOne.tag == 1
        {
            buttonOneImage.image = UIImage(named: "option_green_bg.png")
            buttonOneLabel.textColor = UIColor.whiteColor()
            buttonOneImage.alpha = 0.0
            UIButton.animateWithDuration(0.5, animations: {
                self.buttonOneImage.alpha = 1.0
                }, completion: {
                    (value: Bool) in
            })

        }else if buttonTwo.tag == 1
        {
            buttonTwoImage.image = UIImage(named: "option_green_bg.png")
            buttonTwoLabel.textColor = UIColor.whiteColor()
            buttonTwoImage.alpha = 0.0
            UIButton.animateWithDuration(0.5, animations: {
                self.buttonTwoImage.alpha = 1.0
                }, completion: {
                    (value: Bool) in
            })
            
        }else if buttonThree.tag == 1
        {
            buttonThreeImage.image = UIImage(named: "option_green_bg.png")
            buttonThreeLabel.textColor = UIColor.whiteColor()
            buttonThreeImage.alpha = 0.0
            UIButton.animateWithDuration(0.5, animations: {
                self.buttonThreeImage.alpha = 1.0
                }, completion: {
                    (value: Bool) in
            })

        }else if buttonFour.tag == 1
        {
            buttonThree.userInteractionEnabled = false
            buttonTwo.userInteractionEnabled = false
            buttonOne.userInteractionEnabled = false
 
            buttonFourImage.image = UIImage(named: "option_green_bg.png")
            buttonFourLabel.textColor = UIColor.whiteColor()
            buttonFourImage.alpha = 0.0
            UIButton.animateWithDuration(0.5, animations: {
                self.buttonFourImage.alpha = 1.0
                }, completion: {
                    (value: Bool) in
            })
        }

    }
    var importedView = UIView()
    var popUpView = KLCPopup()
    
    @IBAction func endquizButton(sender: UIButton) {
        
        let user_arr : NSArray = NSBundle.mainBundle().loadNibNamed("ExitView", owner: self, options: nil)!  as NSArray
        let view :UIView = user_arr.objectAtIndex(0) as! UIView
                let yesBtn = (view.viewWithTag(2)! as! UIButton)
                let noBtn = (view.viewWithTag(1)! as! UIButton)
                let label = (view.viewWithTag(3)! as! UILabel)
                yesBtn.addTarget(self, action: #selector(QuizViewController.submit(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                noBtn.addTarget(self, action: #selector(QuizViewController.cancel(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                popUpView = KLCPopup(contentView: view , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: false, dismissOnContentTouch: false)
                popUpView.show()
        
        }
    
    func submit(sender: UIButton){
        self.navigationController?.popViewControllerAnimated(false)
        popUpView.dismiss(true)
        popUpView.dismissPresentingPopup()
    }
    
    func cancel(sender: UIButton){
         popUpView.dismiss(true)
        popUpView.dismissPresentingPopup()
    }
    
    @IBAction func ansCountButton(sender: UIButton) {
        
    }
    @IBAction func backButton(sender: UIBarButtonItem) {
       
        
    }
}
