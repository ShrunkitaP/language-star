//
//  AlphabetsTableViewCell.swift
//  Learning App
//
//  Created by HPL on 11/08/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit

class AlphabetsTableViewCell: UITableViewCell {

    
    @IBOutlet var imgWord: UIImageView!
    @IBOutlet var lblEnglishWord: UILabel!
    @IBOutlet var lblWordPronunciation: UILabel!
    @IBOutlet weak var PlayButtonTapped: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
