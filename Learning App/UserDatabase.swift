//
//  UserDatabase.swift
//  Learning App
//
//  Created by HPL on 24/10/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit
import CoreData


class UserDatabase: UIViewController {
    
    var data = [NSManagedObject]()
    
    
    override func viewWillAppear(animated: Bool){
        
        super.viewWillAppear(animated)
        
    }
    
    func saveName(starKey: String, langKey:String, levelKey:String, quizKey: String , medalKey: String) {
        
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("UserData",
            inManagedObjectContext:managedContext)
        
        let star = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedContext)
        
        
        let fetchRequest = NSFetchRequest(entityName: "UserData")
        
        fetchRequest.predicate = NSPredicate(format: "(lang_id = %@) AND (quiz_no = %@)", langKey, quizKey)
        
        
        var results = NSArray()
        
        
        do {
            results = try managedContext.executeFetchRequest(fetchRequest)
            
            //            try managedContext.save()
       
            if(results.count < 1)
             {
                //3
                star.setValue(starKey, forKey: "star")
                star.setValue(langKey, forKey: "lang_id")
                star.setValue(levelKey, forKey: "level_id")
                star.setValue(quizKey, forKey: "quiz_no")
                star.setValue(medalKey, forKey: "medal")
                //4
                do {
                    try managedContext.save()
                    
                    print(self.fetchAllRecord(langKey))
                    
                    
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
          }
        }
        catch let error
        {
            print(error)
        }
 }
    
    
    func fetchAllRecord(object_id: NSString) -> NSArray
    {
        var userData:NSArray = NSArray ()
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        
        let fetchRequest = NSFetchRequest(entityName: "UserData")
        
      fetchRequest.predicate = NSPredicate(format: "(lang_id = %@)", object_id)
    
        do {
            userData = try managedContext.executeFetchRequest(fetchRequest)
            print(userData)
//            try managedContext.save()
        }
        catch let error {
            print(error)
        }
        print(userData)
        return userData
    }
    
    func fetchStardata(starKey: String, langKey:String, levelKey:String, quizKey: String , medal: String) -> NSArray
    {
        var userData:NSArray = NSArray ()
        //1
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        
        let fetchRequest = NSFetchRequest(entityName: "UserData")
        
        fetchRequest.predicate = NSPredicate(format: "(lang_id = %@) AND (quiz_no = %@)",langKey, quizKey)
        
        do {
            userData = try managedContext.executeFetchRequest(fetchRequest)
            print(userData)
        }
        catch let error {
            print(error)
        }
        return userData

    }
    
    func fetchPopupData(starKey: String, langKey:String, levelKey:String, quizKey: String) -> NSArray
    {
        var userData:NSArray = NSArray ()
        //1
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        
        let fetchRequest = NSFetchRequest(entityName: "UserData")
        
        fetchRequest.predicate = NSPredicate(format: "(lang_id = %@) AND (level_id = %@)",langKey, levelKey)
        
        do {
            userData = try managedContext.executeFetchRequest(fetchRequest)
            print(userData)
        }
        catch let error {
            print(error)
        }
        return userData
        
    }

    
    
    func updateTable(object_id: NSString,stars : String, langid : String , medal: String) -> Bool
    {
        print(medal)
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("UserData",
                    inManagedObjectContext:managedContext)
        
        let star = NSManagedObject(entity: entity!,
                    insertIntoManagedObjectContext: managedContext)
        
        let fetchRequest = NSFetchRequest(entityName: "UserData")
        fetchRequest.predicate = NSPredicate(format: "(lang_id = %@) AND (quiz_no = %@)", langid,object_id)
        
        var results = NSArray()
        
        do {
            results = try managedContext.executeFetchRequest(fetchRequest)
            
             if (results.count > 0)
             {
                
               let star = results.objectAtIndex(0)
              
               star.setValue(stars, forKey: "star")
            star.setValue(medal, forKey: "medal")
                
                do
                {
                    try managedContext.save()
                    return true
                    
                    
                 } catch let error as NSError
                 {
                    print("Could not save \(error), \(error.userInfo)")
                     return false
                }
               }
             else{
               return false
            }
          }
        catch let error
        {
          return false
        }
        
       
    }
    
    
    func deleteUserData(object_id: NSString) -> Bool
    {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("UserData",
            inManagedObjectContext:managedContext)
        
        let star = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedContext)
        

        let fetchRequest = NSFetchRequest(entityName: "UserData")
        fetchRequest.predicate = NSPredicate(format: "(quiz_no = %@)", object_id)
//        fetchRequest.predicate = NSPredicate(format: "(star = %@)", object_id)
        fetchRequest.returnsObjectsAsFaults = false
        do{
            let fetchResults = try managedContext.executeFetchRequest(fetchRequest)
            if fetchResults.count>0
            {
                managedContext.deleteObject(fetchResults[0] as! NSManagedObject)
            }
            else
            {
                // no data
            }
            do {
                try managedContext.save()
            } catch {
                print(error)
            }
            
            return true
        }
        catch
        {
            return false
        }
    }

}


