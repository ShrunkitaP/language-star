//
//  CustomClass.swift
//  Learning App
//
//  Created by Riken Shah on 24/08/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit

class CustomClass: NSObject {
    
    func displayMyAlertMessage(userMessage:String) -> UIAlertController {
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.Alert);
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil);
        myAlert.addAction(okAction);
        
        return myAlert
    }


}
