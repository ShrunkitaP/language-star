//
//  SelectLanguageViewController.swift
//  Learning App
//
//  Created by Riken Shah on 25/08/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit
import Alamofire

class SelectLanguageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myTableView: UITableView!
     var appdelegate: AppDelegate?
     var languageData : NSMutableArray = NSMutableArray()
    //var userDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.myTableView.delegate = self
        self.myTableView.dataSource = self
        self.webServiceCallforFetchingLanguageData()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewWillAppear(animated: Bool)
//    {
//        self.webServiceCallforFetchingLanguageData()
//    }
    
    
    func webServiceCallforFetchingLanguageData()
    {
        
        //appdelegate!.showHUD(self.view)
        Alamofire.request(.GET, "http://172.16.2.17/dictionary/public/laguage-list")
            .responseJSON { response in
                //                print(response.request)  // original URL request
                //                print(response.response) // URL response
                //                print(response.data)     // server data
                //                print(response.result)   // result of response serialization
                if let JSON = response.result.value
                {
                    //                    print("JSON: \(JSON)")
                    //self.appdelegate?.dismissView(self.view)
                    self.languageData.setArray(JSON.objectForKey("data")! as! [AnyObject])
                    print("JSON: \(self.languageData)")
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                           self.myTableView.reloadData()
                    })


                    
                }
        }
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.languageData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
         let cell = tableView.dequeueReusableCellWithIdentifier("language", forIndexPath: indexPath) as! LanguageTableViewCell
        
        cell.AllLanguage.text = self.languageData.objectAtIndex(indexPath.row).valueForKey("language") as? String
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        print(self.languageData.objectAtIndex(indexPath.row).valueForKey("language"))
        
        //userDefaults.setObject(self.languageData.objectAtIndex(indexPath.row).valueForKey("language"), forKey: "NewLanguage")
        NSUserDefaults.standardUserDefaults().setObject(self.languageData.objectAtIndex(indexPath.row).valueForKey("language"), forKey: "NewLanguage")
        NSUserDefaults.standardUserDefaults().setObject(self.languageData.objectAtIndex(indexPath.row).valueForKey("id"), forKey: "NewLangId")
        self.navigationController?.popViewControllerAnimated(true)
        //.setObject(token, forKey: token as String)
   }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
