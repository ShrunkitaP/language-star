//
//  SettingsViewController.swift
//  Learning App
//
//  Created by HPL on 18/08/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit
import Alamofire
import SWRevealViewController

class SettingsViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var viewNativeLanguage: UIView!
    @IBOutlet var lblSelectedNativeLanguage: UILabel!
    @IBOutlet weak var tableViewLanguages: UITableView!
   
    //var preefed : String = ""
    //var tableArray = [String]()
    var addingLanguage : String = ""
   var langCode: String = ""
     var selectedRow : Int?
    var languageData : NSMutableArray = NSMutableArray()
    var finalLanguageData : NSMutableArray = NSMutableArray()
     var checkquiz : String?
    var appdelegate: AppDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        
        // Do any additional setup after loading the view.
        //tableArray += [preefed]
        
        //print(tableArray)
        self.navigationItem.title = "Settings"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.whiteColor(),
             NSFontAttributeName: UIFont(name: "HVDComicSerifPro", size: 21)!]
        
        viewNativeLanguage.layer.cornerRadius = 5
        viewNativeLanguage.layer.masksToBounds = true
        lblSelectedNativeLanguage.text! = (NSUserDefaults.standardUserDefaults().objectForKey("Language") as? String)!
        self.webServiceCallforFetchingLanguageData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
          super.viewDidAppear(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        //addingLanguage = NSUserDefaults.standardUserDefaults().stringForKey("NewLanguage")!
        //print(addingLanguage)
        //tableArray += [addingLanguage]
        
        super.viewWillAppear(true)
    }
    
    func webServiceCallforFetchingLanguageData()
    {
        
        //appdelegate!.showHUD(self.view)
        if Reachability.isConnectedToNetwork() == true
        {
            Alamofire.request(.GET, "http://languagestar.net/admin/public/laguage-list")
            .responseJSON { _, _, result in
                //                print(response.request)  // original URL request
                //                print(response.response) // URL response
                //                print(response.data)     // server data
                //                print(response.result)   // result of response serialization
                if let JSON = result.value
                {
                    //                    print("JSON: \(JSON)")
                    //self.appdelegate?.dismissView(self.view)
                    self.languageData.setArray(JSON.objectForKey("data")! as! [AnyObject])
                    print("JSON: \(self.languageData)")
                    
                    self.tableViewLanguages.dataSource = self
                    self.tableViewLanguages.delegate = self

                    dispatch_async(dispatch_get_main_queue(),
                        {
                           self.tableViewLanguages.reloadData()

                            
                            //let tableViewLanguages = UITableView(frame: CGRect(x: 0,y: 0,width: Int(self.viewTableDisplay.frame.width),height:44*self.languageData.count))
                            self.tableViewLanguages.layer.cornerRadius = 5
                            self.tableViewLanguages.layer.masksToBounds = true
                            
                                                   //self.tableViewLanguages.scrollEnabled = false
                            //self.viewTableDisplay.addSubview(tableViewLanguages)

                    })
                    
                }else{
                    self.appdelegate?.dismissView(self.view)
                    let Cust = CustomClass()
                    var alert = UIAlertController( )
                    alert = Cust.displayMyAlertMessage("No data found")
                    self.presentViewController(alert, animated: true, completion: nil)
                }
        }
        }else{
            self.appdelegate?.dismissView(self.view)
            let Cust = CustomClass()
            var alert = UIAlertController( )
            alert = Cust.displayMyAlertMessage("Please check your internet connection.")
            self.presentViewController(alert, animated: true, completion: nil)

        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func pickLanguageData()
    {
        
        //   sum = languageData.objectAtIndex(i).valueForKey("Count") as! Int
        for i in 0...languageData.count-1 {
            if (languageData.objectAtIndex(i).valueForKey("Count") as! String != "0"){
                finalLanguageData.addObject(languageData.objectAtIndex(i))
                print(finalLanguageData)
                
            }
            else
            {
                languageData.removeObject(i)
            }
        }
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        pickLanguageData()
         return self.finalLanguageData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        
        cell.backgroundColor = UIColor.whiteColor()
        
        cell.textLabel?.text = self.finalLanguageData.objectAtIndex(indexPath.row).valueForKey("language") as? String
        cell.textLabel?.font = UIFont(name: "HVDComicSerifPro", size: 17)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        addingLanguage = self.finalLanguageData.objectAtIndex(indexPath.row).valueForKey("language") as! String
        print(addingLanguage)
         selectedRow = indexPath.row
       // langCode = self.languageData.objectAtIndex(indexPath.row).objectForKey("id")!
        //userDefaults.setObject(self.languageData.objectAtIndex(indexPath.row).valueForKey("language"), forKey: "NewLanguage")
        //NSUserDefaults.standardUserDefaults().setObject(self.languageData.objectAtIndex(indexPath.row).valueForKey("language"), forKey: "NewLanguage")
        NSUserDefaults.standardUserDefaults().setValue(self.finalLanguageData.objectAtIndex(indexPath.row).valueForKey("language"), forKey: "Language")
        lblSelectedNativeLanguage.text! = NSUserDefaults.standardUserDefaults().objectForKey("Language") as! String
//        NSUserDefaults.standardUserDefaults().setObject(self.languageData.objectAtIndex(indexPath.row).valueForKey("id"), forKey: "LanguageCode")
        NSUserDefaults.standardUserDefaults().setValue(String(self.finalLanguageData.objectAtIndex(indexPath.row).valueForKey("id") as! NSNumber), forKeyPath: "lang_id")
         NSUserDefaults.standardUserDefaults().setValue(self.finalLanguageData.objectAtIndex(indexPath.row).valueForKey("Count") as! NSString, forKeyPath: "quiz_no")
        
      checkquiz = finalLanguageData.objectAtIndex(indexPath.row).objectForKey("Count") as? String
        print(checkquiz)
        NSUserDefaults.standardUserDefaults().setObject(checkquiz, forKey: "quiz_no")

        NSUserDefaults.standardUserDefaults().synchronize()
    }
//    @IBAction func btnNativeLanguageAction(sender: AnyObject) {
//        
//         performSegueWithIdentifier("ShowAllLanguage", sender: nil)
//        
//    }
    @IBAction func btnBackAction(sender: UIBarButtonItem) {
        print(finalLanguageData)
        print(langCode)
        if self.selectedRow != nil {
            langCode = String(self.finalLanguageData.objectAtIndex(self.selectedRow!).objectForKey("id") as! Int)
            let userdefaults = NSUserDefaults.standardUserDefaults()
            langCode = userdefaults.objectForKey("lang_id") as! String
            checkquiz = finalLanguageData.objectAtIndex(self.selectedRow!).objectForKey("Count") as? String
        } else {
            let userdefaults = NSUserDefaults.standardUserDefaults()
            langCode = userdefaults.objectForKey("lang_id") as! String
            print(langCode)
            checkquiz = userdefaults.objectForKey("quiz_no") as? String
            addingLanguage = userdefaults.objectForKey("Language") as! String
            print(addingLanguage)
            print(checkquiz)
            userdefaults.synchronize()
        //   langCode = UserDefaults
            
        }
        
//        var checkquiz : Int?
//        checkquiz = checkQuiz()
//        print(checkquiz)
//        if (checkquiz >= 48){
//            let userdefaults = NSUserDefaults.standardUserDefaults()
//            userdefaults.setValue(String(langCode), forKey: "LanguageCode")
//            userdefaults.setValue(checkquiz, forKey: "Count")
//            userdefaults.synchronize()
//            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
//            self.navigationController?.pushViewController(destination, animated: true)
//            self.navigationController?.navigationBarHidden = true
//        }
//        else
//        {
//            let Cust = CustomClass()
//            var alert = UIAlertController( )
//            alert = Cust.displayMyAlertMessage("Quiz not available for \(lblSelectedNativeLanguage.text!) Language")
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }
        
        let destination = self.storyboard?.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.navigationController?.pushViewController(destination, animated: false)
        self.navigationController?.navigationBarHidden = true
    }
    
    func checkQuiz() -> Int{
        for each in finalLanguageData
        {
            if ((each as! NSDictionary).objectForKey("id") as? Int)! == Int(langCode)
            {
                return (each as! NSDictionary).objectForKey("Count") as! Int
            }
        }
        return 0
    }
}
