//
//  ImageCollectionViewCell.swift
//  Learning App
//
//  Created by HPL on 12/08/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgAlphabet: UIImageView!
    @IBOutlet var lblAlphabet: UILabel!
}
