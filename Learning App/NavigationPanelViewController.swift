//
//  NavigationPanelViewController.swift
//  Learning App
//
//  Created by ospmac on 17/11/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit
import Alamofire

class NavigationPanelViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var navigationPanel: UICollectionView!
    let activityOptions = ["PLAY QUIZ","SETTINGS"]
    var appdelegate: AppDelegate?
    var languageData : NSMutableArray = NSMutableArray()
    var finalLanguageData : NSMutableArray = NSMutableArray()
    var languageCode = String()
    var language = String()
      var checkquiz : Int?
    var params:NSDictionary = NSDictionary()
    var jsonData : NSDictionary = NSDictionary()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userdefault = NSUserDefaults.standardUserDefaults()
        self.languageCode = userdefault.objectForKey("lang_id") as! String
        self.checkquiz = Int((userdefault.objectForKey("quiz_no")! as? String)!)
        self.language = userdefault.objectForKey("Language") as! String
        print(language)
        print(languageCode)
        print(checkquiz)
        // Do any additional setup after loading the view.
        navigationPanel.registerNib(UINib(nibName: "NavigationPanelCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NavigationPanelCell")
         appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        let userdefault = NSUserDefaults.standardUserDefaults()
        self.languageCode = userdefault.objectForKey("lang_id") as! String
        self.checkquiz = Int((userdefault.objectForKey("quiz_no")! as? String)!)
        self.language = userdefault.objectForKey("Language") as! String
        print(checkquiz)

//self.webServiceCallforFetchingLanguageData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activityOptions.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("NavigationPanelCell", forIndexPath: indexPath) as! NavigationPanelCollectionViewCell
        cell.lblActivity.text = activityOptions[indexPath.item]
        cell.imgActivity.image = UIImage(named: activityOptions[indexPath.item])
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.width)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.revealViewController().revealToggleAnimated(true)
        switch indexPath.row
        {
        case 0:
            let deviceType = UIDevice.currentDevice().model
            if (deviceType == "iPhone") {
                // for i in 0...languageData.count-1 {
                if checkquiz >= 48 {
                    let userdefaults = NSUserDefaults.standardUserDefaults()
                    userdefaults.setValue(String(languageCode), forKey: "LanguageCode")
                    userdefaults.synchronize()
                    //         languageCode = self.finalLanguageData.objectAtIndex(i).objectForKey("id")! as! String
                    let destination = self.storyboard?.instantiateViewControllerWithIdentifier("DashboardViewController") as! DashboardScreenViewController
                    let nav = UINavigationController(rootViewController: destination)
                    self.revealViewController().addChildViewController(nav)
                    self.revealViewController().setFrontViewController(nav, animated: true)
                    
                }
                else
                {
                    let Cust = CustomClass()
                    var alert = UIAlertController( )
                    alert = Cust.displayMyAlertMessage("Quiz not available for \(language) Language")
                    self.presentViewController(alert, animated: true, completion: nil)
                    let destination = self.storyboard?.instantiateViewControllerWithIdentifier("LandingPage") as! AlphabetsViewController
                    self.navigationController?.popToViewController(destination, animated: true)
                    //let nav = UINavigationController(rootViewController: destination)
                    //    self.revealViewController().addChildViewController(destination)
                    //     self.revealViewController().setFrontViewController(destination, animated: true)
                    
                }
                //  }
                
                
            } else {
                //for i in 0...languageData.count-1 {
                if checkquiz >= 48 {
                    let userdefaults = NSUserDefaults.standardUserDefaults()
                    userdefaults.setValue(String(languageCode), forKey: "LanguageCode")
                    userdefaults.synchronize()
                    let destination = self.storyboard?.instantiateViewControllerWithIdentifier("DashboardIpad") as! DashboardScreenViewController
                    let nav = UINavigationController(rootViewController: destination)
                    self.revealViewController().addChildViewController(nav)
                    self.revealViewController().setFrontViewController(nav, animated: true)
                }
                else
                {
                    let Cust = CustomClass()
                    var alert = UIAlertController( )
                    alert = Cust.displayMyAlertMessage("Quiz not available for \(language) Language")
                    self.presentViewController(alert, animated: true, completion: nil)
                    let destination = self.storyboard?.instantiateViewControllerWithIdentifier("LandingPage") as! AlphabetsViewController
                    self.navigationController?.popToViewController(destination, animated: true)
                    //  self.revealViewController().addChildViewController(destination)
                    self.revealViewController().setFrontViewController(destination, animated: true)
                    
                    //   }
                }
                
                //                        let destination = self.storyboard?.instantiateViewControllerWithIdentifier("DashboardIpad") as! DashboardScreenViewController
                //                        let nav = UINavigationController(rootViewController: destination)
                //                        self.revealViewController().addChildViewController(nav)
                //                        self.revealViewController().setFrontViewController(nav, animated: true)
            }
            
            break
        case 1:
            let destination = self.storyboard?.instantiateViewControllerWithIdentifier("SettingsViewController") as! UINavigationController
            self.revealViewController().addChildViewController(destination)
            self.revealViewController().setFrontViewController(destination, animated: true)
            break
        default:
            break
        }
    }

//        self.revealViewController().revealToggleAnimated(true)
////        params = ["native_lang_id" : languageCode, "level" : "intermediate"]
////        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
////        let session = NSURLSession(configuration: configuration)
//        switch indexPath.row
//                {
//                case 0:
//                    let deviceType = UIDevice.currentDevice().model
//                    if (deviceType == "iPhone") {
//                     
//                            if checkquiz > 48 {
//                                let userdefaults = NSUserDefaults.standardUserDefaults()
//                                userdefaults.setValue(String(languageCode), forKey: "lang_id")
//                                userdefaults.synchronize()
//                       //         languageCode = self.finalLanguageData.objectAtIndex(i).objectForKey("id")! as! String
//                                let destination = self.storyboard?.instantiateViewControllerWithIdentifier("DashboardViewController") as! DashboardScreenViewController
//                                let nav = UINavigationController(rootViewController: destination)
//                                self.revealViewController().addChildViewController(nav)
//                                self.revealViewController().setFrontViewController(nav, animated: true)
//
//                            }
//                            else
//                            {
//                                let Cust = CustomClass()
//                                var alert = UIAlertController( )
//                                alert = Cust.displayMyAlertMessage("Quiz not available for \(language) Language")
//                                self.presentViewController(alert, animated: true, completion: nil)
//                                let destination = self.storyboard?.instantiateViewControllerWithIdentifier("LandingPage") as! AlphabetsViewController
//                                self.navigationController?.popToViewController(destination, animated: true)
//                                //let nav = UINavigationController(rootViewController: destination)
//                            //    self.revealViewController().addChildViewController(destination)
//                           //     self.revealViewController().setFrontViewController(destination, animated: true)
//
//                            }
////                        appdelegate!.showHUD(self.view)
////                        Alamofire.request(.POST, "http://bettie.test.heypayless.com/dictionary/public/getQuestionList", parameters: ["native_lang_id" : languageCode,"level" : "intermediate"])
////                            .responseJSON { _, _, result in
////                                //                print(result.request)  // original URL request
////                                //                print(result.response) // URL response
////                                //                print(result.data)     // server data
////                                //                print(result.result)   // result of response serialization
////                                
////                                if let JSON = result.value
////                                {
////                                    print("JSON: \(JSON)")
////                                    self.appdelegate?.dismissView(self.view)
////                                    if (JSON.objectForKey("data") == nil){
////                                        let Cust = CustomClass()
////                                        var alert = UIAlertController( )
////                                        alert = Cust.displayMyAlertMessage("Quiz not available for \(self.language) Language")
////                                        self.presentViewController(alert, animated: true, completion: nil)
////                                        let destination = self.storyboard?.instantiateViewControllerWithIdentifier("LandingPage") as! AlphabetsViewController
////                                        self.navigationController?.popToViewController(destination, animated: true)
////                                        //  self.revealViewController().addChildViewController(destination)
////                                        //self.revealViewController().setFrontViewController(destination, animated: true)
////                                    }
////                                    else{
////                                        let userdefaults = NSUserDefaults.standardUserDefaults()
////                                        userdefaults.setValue(String(self.languageCode), forKey: "lang_id")
////                                        userdefaults.synchronize()
////                                        let destination = self.storyboard?.instantiateViewControllerWithIdentifier("DashboardIpad") as! DashboardScreenViewController
////                                        let nav = UINavigationController(rootViewController: destination)
////                                        self.revealViewController().addChildViewController(nav)
////                                        self.revealViewController().setFrontViewController(nav, animated: true)
////
////
////                                    }
////                                }
////                        
////                        }
//                        
//                    }else {
//                        appdelegate!.showHUD(self.view)
//                        Alamofire.request(.POST, "http://bettie.test.heypayless.com/dictionary/public/getQuestionList", parameters: ["native_lang_id" : languageCode,"level" : "intermediate"])
//                            .responseJSON { _, _, result in
//                                //                print(result.request)  // original URL request
//                                //                print(result.response) // URL response
//                                //                print(result.data)     // server data
//                                //                print(result.result)   // result of response serialization
//
//                        if let JSON = result.value
//                        {
//                            print("JSON: \(JSON)")
//                            self.appdelegate?.dismissView(self.view)
//                            if (JSON.objectForKey("data") == nil){
//                                let Cust = CustomClass()
//                                var alert = UIAlertController( )
//                                alert = Cust.displayMyAlertMessage("Quiz not available for \(self.language) Language")
//                                self.presentViewController(alert, animated: true, completion: nil)
//                                let destination = self.storyboard?.instantiateViewControllerWithIdentifier("LandingPage") as! AlphabetsViewController
//                                self.navigationController?.popToViewController(destination, animated: true)
//                                //  self.revealViewController().addChildViewController(destination)
//                                //self.revealViewController().setFrontViewController(destination, animated: true)
//                            }
//                            else{
//                                let userdefaults = NSUserDefaults.standardUserDefaults()
//                                userdefaults.setValue(String(self.languageCode), forKey: "lang_id")
//                                userdefaults.synchronize()
//                                let destination = self.storyboard?.instantiateViewControllerWithIdentifier("DashboardIpad") as! DashboardScreenViewController
//                                let nav = UINavigationController(rootViewController: destination)
//                                self.revealViewController().addChildViewController(nav)
//                                self.revealViewController().setFrontViewController(nav, animated: true)
//                                
//                                
//                            }
//                        }
//                        
//        
//
////                        let destination = self.storyboard?.instantiateViewControllerWithIdentifier("DashboardIpad") as! DashboardScreenViewController
////                        let nav = UINavigationController(rootViewController: destination)
////                        self.revealViewController().addChildViewController(nav)
////                        self.revealViewController().setFrontViewController(nav, animated: true)
//                      }
//                    }
//                    
//                    break
//                case 1:
//                    let destination = self.storyboard?.instantiateViewControllerWithIdentifier("SettingsViewController") as! UINavigationController
//                    self.revealViewController().addChildViewController(destination)
//                    self.revealViewController().setFrontViewController(destination, animated: true)
//                    break
//                default:
//                    break
//                }
    }


