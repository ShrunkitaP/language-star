//
//  dashboradCell.swift
//  Learning App
//
//  Created by HPL on 07/10/16.
//  Copyright © 2016 heypayless. All rights reserved.
//

import UIKit

class dashboradCell: UITableViewCell {
    
    
    @IBOutlet var dashboardLevelView: UIView!
    @IBOutlet var levelLabel: UILabel!
    @IBOutlet var medalImage: UIImageView!
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button1SmallStar1: UIImageView!
    @IBOutlet var button1BigStar1: UIImageView!
    @IBOutlet var button1SmallStar2: UIImageView!
    @IBOutlet var button1Timer: UIImageView!
    @IBOutlet var button2SmallStar1: UIImageView!
    @IBOutlet var button2BigStar1: UIImageView!
    @IBOutlet var button2SmallStar2: UIImageView!
    @IBOutlet var button2Timer: UIImageView!
    @IBOutlet var button3SmallStar1: UIImageView!
    @IBOutlet var button3BigStar1: UIImageView!
    @IBOutlet var button3SmallStar2: UIImageView!
    @IBOutlet var button3Timer: UIImageView!
    @IBOutlet var buttonOneImage: UIImageView!
    @IBOutlet var buttonTwoImage: UIImageView!
    @IBOutlet var buttonThreeImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
