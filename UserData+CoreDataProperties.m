//
//  UserData+CoreDataProperties.m
//  Learning App
//
//  Created by HPL on 15/12/16.
//  Copyright © 2016 heypayless. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserData+CoreDataProperties.h"

@implementation UserData (CoreDataProperties)

@dynamic lang_id;
@dynamic level_id;
@dynamic medal;
@dynamic quiz_no;
@dynamic star;

@end
